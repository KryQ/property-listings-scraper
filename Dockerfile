FROM node:22.0.0-bullseye-slim

RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app
RUN apt-get update || : && apt-get install python3 -y

WORKDIR /home/node/app

RUN mkdir {backend,frontend}

COPY ./package*.json ./
COPY ./frontend/package*.json ./frontend/
COPY ./backend/package*.json ./backend/

RUN npm ci

WORKDIR /home/node/app/frontend
RUN npm ci

WORKDIR /home/node/app/backend
RUN npm ci

WORKDIR /home/node/app

USER node

COPY --chown=node:node . .

RUN npm run transpile-common

WORKDIR /home/node/app/frontend
RUN npm run build

WORKDIR /home/node/app/backend
RUN npm run transpile
RUN npm run fetchFrontend

EXPOSE 3000

CMD [ "node", "./dist/index.js" ]

