In order to start the app on host machine use foreman.
In order for this to work you need to setup `.env` file in backend directory with following variables:

* DB_HOST - addres to db
* DB_PORT - port db is listening on
* DB_USER - db user
* DB_PASSWORD - db password
* DB_NAME - db name
* REST_KEY - key for rest operation (currently creating users)
* JWT_SECRET - hash salt for JWT 
* LOKI_KEY - token for loki transport

In my instance `forego start`


For docker start you can run `docker compose -f docker-compose.yml up` from main folder. Docker should create db for you and build local image to work with.
