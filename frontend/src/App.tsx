import {
	CssBaseline,
	type PaletteMode,
	ThemeProvider,
	createTheme,
	useMediaQuery,
	Box,
} from "@mui/material";
import React, { useRef } from "react";
import { Route, Switch } from "wouter";
import { ListingDetails } from "./pages/ListingDetails.tsx";
// @ts-ignore
import { Listings } from "./pages/Listings.jsx";
// @ts-ignore
import { Layout } from "./Layout.jsx";
// @ts-ignore
import { useListingsStore } from "./store/listings";
// @ts-ignore
import { useNotificationStore } from "./store/notification";
import { isProdBuild } from "./utils.ts";
import { useUserStore } from "./store/user.ts";
import { UserSettings } from "./pages/UserSettings.tsx";

export const App = () => {
	const { check: checkUser } = useUserStore();
	const { fetchListings } = useListingsStore();
	const { fetchNotifications } = useNotificationStore();
	const isSystemDarkMode = useMediaQuery("(prefers-color-scheme: dark)");
	const notificationCheckHandler = useRef<number | null>(null);

	const themeProvider = React.useMemo(
		() =>
			createTheme({
				palette: {
					mode: isSystemDarkMode ? "dark" : ("light" as PaletteMode),
				},
			}),
		[isSystemDarkMode],
	);

	const initData = async () => {
		fetchListings();
		const isUserLoggedIn = await checkUser();
		if (isUserLoggedIn) {
			fetchNotifications();
			notificationCheckHandler.current = setInterval(() => {
				fetchNotifications();
			}, 60 * 1000);
		}
	};

	// biome-ignore lint/correctness/useExhaustiveDependencies: <explanation>
	React.useEffect(() => {
		initData();
		return () => {
			notificationCheckHandler.current !== null &&
				clearInterval(notificationCheckHandler.current);
		};
	}, []);

	return (
		<ThemeProvider theme={themeProvider}>
			<Box sx={{ my: 2 }}>
				{!isProdBuild && (
					<Box sx={{ position: "fixed", bottom: 0, right: 0 }}>DEV BUILD</Box>
				)}
				<CssBaseline enableColorScheme />
				<Switch>
					<Layout>
						<Route path="/" component={Listings} />
						<Route path="/favourites">
							<Listings favoritesOnly />
						</Route>

						<Route path="/listing/:id" component={ListingDetails} />
						<Route path="/user-settings" component={UserSettings} />
					</Layout>
				</Switch>
			</Box>
		</ThemeProvider>
	);
};
