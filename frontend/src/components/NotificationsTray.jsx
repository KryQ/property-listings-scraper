import { AddHome, Delete, Unsubscribe, Update } from "@mui/icons-material"
import { List, ListItemButton, Stack, Button, ListItemText, ListItemIcon, ListItem, IconButton, Typography } from "@mui/material"
import { useState } from "react";
import { useNotificationStore } from "../store/notification";
import { Link } from "wouter";

const getIcon = (type) => {
  switch (type) {
    case 'DELETED': return <Delete />;
    case 'NEW': return <AddHome />;
    case 'CHANGED': return <Update />;
  }
}

const getStatus = (notification) => {
  if (notification.type !== 'CHANGED') {
    return undefined;
  }

  return `Changes: ${notification.listingChange.changes.map(change => change.path.join('/')).join(', ')}`
}

export const NotificationsTray = () => {
  const { notifications, markAsSeen, markAllAsSeen } = useNotificationStore();
  const [hideSeen, setSeenVisibility] = useState(true);

  const visibleNotifications = hideSeen ? notifications.filter(({ seenAt }) => !seenAt) : notifications;

  return (
    <Stack direction="column" sx={{ minWidth: '400px' }}>
      <List sx={{ maxHeight: '400px', overflowY: 'auto' }}>
        {visibleNotifications.length === 0 ? (
          <Typography variant="body1" sx={{ color: "slategray", width: '100%', textAlign: 'center' }}>
            No new notifications
          </Typography>
        ) : (
          visibleNotifications.map(notification => (
            <ListItem
              key={notification.id}
              secondaryAction={
                <IconButton onClick={() => markAsSeen(notification.id)}><Unsubscribe /></IconButton>
              }>
              <ListItemButton component={Link} to={`/listing/${notification.listing.id}`}>
                <ListItemIcon>{getIcon(notification.type)}</ListItemIcon>
                <ListItemText primary={notification.listing.title} secondary={getStatus(notification)} />
              </ListItemButton>
            </ListItem>
          ))
        )}
      </List>
      <Stack direction="row" sx={{ mt: 2 }}>
        <Button variant="text" onClick={markAllAsSeen}>Mark all as seen</Button>
        <Button variant="text" onClick={() => { setSeenVisibility(prev => !prev) }}>{hideSeen ? 'Show' : 'Hide'} seen</Button>
      </Stack>
    </Stack>
  )
}