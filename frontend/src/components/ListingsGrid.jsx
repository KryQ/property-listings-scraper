import { Grid } from "@mui/material"
import { ListingCard } from './ListingCard'

export const ListingsGrid = ({ listings, loading }) => {
  return (
    <Grid container spacing={2}>
      {listings.map(listing => (
        <Grid item xs={12} md={6} key={listing.id}>
          <ListingCard listing={listing} />
        </Grid>
      ))}
    </Grid>
  )
}