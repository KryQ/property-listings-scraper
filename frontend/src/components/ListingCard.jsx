import { Button, Card, CardActions, CardContent, CardMedia, Typography, Link } from "@mui/material";
import { OpenInNew } from '@mui/icons-material';

export const ListingCard = ({ listing }) => {
  const {
    title,
    images, link, areaSquareMeters, terrainSquareMeters, address, price, listingCreatedAt } = listing;

  return (
    <Card>
      <CardMedia
        sx={{ height: '300px' }}
        image={images.at(0)}
      />
      <CardContent>
        <Typography variant="h6">
          {title}
        </Typography>
        <Typography>
          Price: {price}
        </Typography>
        <Typography>
          Address:
        </Typography>
        <Typography>
          SquareMeters: {areaSquareMeters}
        </Typography>
        <Typography>
          Terrain: {terrainSquareMeters}
        </Typography>
        <Typography>
          Created at: {listingCreatedAt}
        </Typography>
      </CardContent>
      <CardActions>
        <Link href={link} target="_blank" rel="noopener">
          <Button startIcon={<OpenInNew />}>Go To Listing</Button>
        </Link>
      </CardActions>

    </Card>
  )
}