import striptags from 'striptags';
import React, { useEffect, useState } from "react";
import {
  TableContainer, Table, TableHead, TableRow, TableCell, TableBody,
  Paper, Typography,
  TablePagination
} from "@mui/material";
import { Link } from 'wouter';

import otodomIcon from '../assets/otodom-icon.png';
import olxIcon from '../assets/olx-logo.png';
import nieruchomosciOnlineIcon from '../assets/nieruchomosci-online-logo.png';
import { chunk } from '../utils';

const getSourceIcon = (source) => {
  switch (source) {
    case 'OTODOM': return otodomIcon;
    case 'OLX': return olxIcon;
    case 'NIERUCHOMOSCI_ONLINE': return nieruchomosciOnlineIcon;
  }
}

const prepareDescription = (description) => {
  const strippedDescription = striptags(description);

  if (strippedDescription.length > 200) {
    return `${strippedDescription.substring(0, 197)}...`;
  }

  return strippedDescription;
}

const ListingRow = ({ listing }) => {
  return (
    <TableRow component={Link} to={`/listing/${listing.id}`} sx={{ cursor: 'pointer', textDecoration: 'none' }}>
      <TableCell>
        <img src={listing.images[0]} alt={listing.source} width={64} height={'auto'} />
      </TableCell>
      <TableCell>
        <Typography variant='body1' sx={{ fontWeight: 'bold', display: 'flex', alignItems: 'center' }}>
          <img src={getSourceIcon(listing.source)} alt={listing.title} width={16} style={{ marginRight: '8px' }} />
          {listing.title}
        </Typography>
        <Typography variant='caption' sx={{ color: 'slategrey' }}>{prepareDescription(listing.shortDescription)}</Typography>
      </TableCell>
      <TableCell>{`${listing.address?.city} ${listing.address?.street || ""}`.trim()}</TableCell>
      <TableCell>
        <Typography variant='body1'>{listing.price}zł</Typography>
        <Typography variant='caption' sx={{ color: 'slategrey' }}>
          {(listing.price / listing.areaSquareMeters).toFixed(2)}zł
        </Typography>
      </TableCell>
      <TableCell>
        <Typography variant='body1'>Nier: {listing.areaSquareMeters}m2</Typography>
        <Typography variant='body2'>Teren: {listing.terrainSquareMeters}m2</Typography>
      </TableCell>
    </TableRow >
  )
}

export const ListingsTable = ({ listings, loading }) => {
  const [page, setPage] = useState(0);
  const [itemsPerPage, setItemsPerPage] = useState(20);
  const pagedListings = chunk(listings, itemsPerPage);

  // biome-ignore lint/correctness/useExhaustiveDependencies: <explanation>
  useEffect(() => {
    setPage(0);
  }, [listings, loading]);

  if (!listings || listings.length === 0 || loading) {
    return <Typography>No listings</Typography>
  }

  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell />
            <TableCell>Tytuł</TableCell>
            <TableCell>Adres</TableCell>
            <TableCell>Cena</TableCell>
            <TableCell>Powierzchnia</TableCell>
          </TableRow>
        </TableHead>

        <TableBody>
          {pagedListings[page].map(listing => <ListingRow key={listing.id} listing={listing} />)}
        </TableBody>
      </Table>
      <TablePagination
        rowsPerPage={itemsPerPage}
        rowsPerPageOptions={[5, 20, 50]}
        count={listings.length}
        page={page}
        onPageChange={(_, value) => setPage(value)}
        onRowsPerPageChange={(event) => { setItemsPerPage(Number.parseInt(event.target.value, 10)) }}
      />
    </TableContainer>
  );
}