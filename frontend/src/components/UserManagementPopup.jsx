
import { useRef, useState } from 'react';
import { IconButton, Popover, Box, TextField, Stack, Typography, Tooltip, Menu, MenuItem, ListItemIcon } from "@mui/material";
import { LoadingButton } from '@mui/lab';
import { Login, Logout, Settings } from '@mui/icons-material';
import { useLocation } from 'wouter'
import { useUserStore } from '../store/user';

export const UserManagementPopup = () => {
  const [error, setError] = useState("");
  const { id, loggingIn, logIn, logout } = useUserStore();
  const [_, setLocation] = useLocation();
  const [loginPopupAnchorEl, setLoginPopupAnchorEl] = useState(null);
  const [anchorEl, setAnchorEl] = useState(null);

  const loginRef = useRef();
  const passwordRef = useRef();

  const loginPopupOpen = Boolean(loginPopupAnchorEl);

  const loginPopupHandleClick = (event) => {
    setLoginPopupAnchorEl(event.currentTarget);
  };

  const loginPopupHandleClose = () => {
    setLoginPopupAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleSubmit = async () => {
    setError("");
    const loginResult = await logIn(loginRef.current.value, passwordRef.current.value)
    if (loginResult) {
      loginPopupHandleClose();
    } else {
      setError("Błędne dane logowania");
    }
  }

  return (
    <>
      {!id ? (
        <Tooltip title="Login">
          <IconButton onClick={loginPopupHandleClick}>
            <Login />
          </IconButton>
        </Tooltip>
      ) : (
        <Tooltip title="Logout">
          <IconButton onClick={handleClick}>
            <Settings />
          </IconButton>
        </Tooltip>
      )}
      <Popover
        open={loginPopupOpen}
        anchorEl={loginPopupAnchorEl}
        onClose={loginPopupHandleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
      >
        <Box sx={{ p: 2, maxWidth: '500px' }}>
          <Stack direction="column" spacing={1}>
            <Typography variant='h6'>Zaloguj</Typography>
            <TextField label="Login" inputRef={loginRef} />
            <TextField label="Password" type="password" inputRef={passwordRef} />
            {!!error && (
              <Typography color="error">
                {error}
              </Typography>
            )}
            <LoadingButton
              type='submit'
              variant='contained'
              loading={loggingIn}
              loadingIndicator="Loading…"
              onClick={handleSubmit}
            >
              Login
            </LoadingButton>
          </Stack>
        </Box>
      </Popover>
      <Menu
        anchorEl={anchorEl}
        id="account-menu"
        open={open}
        onClose={handleClose}
        onClick={handleClose}
        slotProps={{
          paper: {
            elevation: 0,
            sx: {
              overflow: 'visible',
              filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
              mt: 1.5,
              '& .MuiAvatar-root': {
                width: 32,
                height: 32,
                ml: -0.5,
                mr: 1,
              },
              '&::before': {
                content: '""',
                display: 'block',
                position: 'absolute',
                top: 0,
                right: 14,
                width: 10,
                height: 10,
                bgcolor: 'background.paper',
                transform: 'translateY(-50%) rotate(45deg)',
                zIndex: 0,
              },
            },
          }
        }}
        transformOrigin={{ horizontal: 'right', vertical: 'top' }}
        anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
      >
        <MenuItem onClick={() => {
          setLocation('/user-settings')
          handleClose();
        }}>
          <ListItemIcon>
            <Settings fontSize="small" />
          </ListItemIcon>
          Settings
        </MenuItem>
        <MenuItem onClick={() => {
          logout()
          handleClose();
        }}>
          <ListItemIcon>
            <Logout fontSize="small" />
          </ListItemIcon>
          Logout
        </MenuItem>
      </Menu>
    </>
  )
}