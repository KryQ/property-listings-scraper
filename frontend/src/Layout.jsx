import { Box, Paper, Stack, IconButton, Badge, Popover, Container, Typography } from "@mui/material"
import { Grid3x3, Home, Notifications, TableChart, Sync, Login, Favorite } from "@mui/icons-material"
import { useState } from "react"
import { Link, useLocation } from "wouter";
import { NotificationsTray } from "./components/NotificationsTray";
import { useNotificationStore } from "./store/notification";
import { useSettingsStore } from "./store/settings";
import { UserManagementPopup } from "./components/UserManagementPopup";
import { useUserStore } from "./store/user";

const SpinningIcon = () => <Sync sx={{
  animation: "spin 2s linear infinite",
  "@keyframes spin": {
    "0%": {
      transform: "rotate(360deg)",
    },
    "100%": {
      transform: "rotate(0deg)",
    },
  },
}}
/>;

export const Layout = ({ children }) => {
  const { notifications, loading } = useNotificationStore();
  const { offersView, changeOffersView } = useSettingsStore();
  const { favourites } = useUserStore();
  const [location, setLocation] = useLocation();
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);

  return (
    <>
      <Container>
        <Stack direction="column" spacing={1}>
          <Paper sx={{ p: 1, justifyContent: 'right' }}>
            <Stack direction="row" gap={2} flex alignItems="center">
              <Typography variant="h6">
                Property Listings Portal
              </Typography>

              {location !== '/' && (
                <IconButton
                  onClick={() => {
                    setLocation('/');
                  }}
                >
                  <Home />
                </IconButton>
              )}
              <Box sx={{ mr: 'auto' }} />
              {location === '/' && (
                <IconButton onClick={() => changeOffersView(offersView === 'TABLE' ? 'GRID' : 'TABLE')}>
                  {offersView === 'TABLE' ? <Grid3x3 /> : <TableChart />}
                </IconButton>
              )}

              <Badge badgeContent={!loading ? notifications.filter(({ seenAt }) => !seenAt).length : undefined} color="info">
                <IconButton onClick={handleClick}>
                  {loading ? <SpinningIcon /> : <Notifications />}
                </IconButton>
              </Badge>

              {favourites.length > 0 && (
                <Badge badgeContent={favourites.length} color="info">
                  <Link to="/favourites">
                    <IconButton>
                      <Favorite />
                    </IconButton>
                  </Link>
                </Badge>
              )}

              <UserManagementPopup />
            </Stack>
          </Paper>
          {children}
        </Stack>
      </Container>
      <Popover
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
      >
        <Box sx={{ p: 1, maxWidth: '500px' }}>
          <NotificationsTray />
        </Box>
      </Popover>
    </>
  )
}