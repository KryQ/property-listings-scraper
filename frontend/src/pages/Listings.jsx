import React from 'react';
import { Paper, Slider, Typography, LinearProgress, TextField, Box, Stack, MenuItem } from '@mui/material';
import debounce from 'debounce';

import { ListingsTable } from '../components/ListingsTable';
import { ListingsGrid } from '../components/ListingsGrid';
import { useSettingsStore } from '../store/settings';
import { useListingsStore } from '../store/listings';
import { useUserStore } from '../store/user';

export const Listings = ({ favoritesOnly = false }) => {
  const { listings, loading, filters, setFilters, sort, setSortOrder, fetchListings } = useListingsStore();
  const { favourites } = useUserStore();
  const { offersView } = useSettingsStore();

  const selectedListings = !favoritesOnly ? listings : listings.filter(listing => favourites.includes(listing.id));

  // biome-ignore lint/correctness/useExhaustiveDependencies: <explanation>
  const debouncedFetch = React.useMemo(() => debounce(fetchListings, 1000), []);

  return (
    <>
      {loading && <LinearProgress />}
      <Paper sx={{ p: 2 }}>
        <Stack direction="column" gap={1} useFlexGap>
          <Typography variant='h5' sx={{ pb: 2 }}>
            Filters
          </Typography>
          <TextField id="outlined-basic" label="Title" variant="outlined" fullWidth
            value={filters.title}
            onChange={(evt) => {
              setFilters({ ...filters, title: evt.target.value })
              debouncedFetch();
            }}
          />
          <Typography>Price</Typography>
          <Box sx={{ px: 1 }}>
            <Slider
              value={filters.price}
              onChange={(_, data) => {
                setFilters({ ...filters, price: data })
                debouncedFetch();
              }}
              min={0}
              max={1999999}
              step={5000}
              valueLabelDisplay="auto" />
          </Box>
          <TextField
            select
            label="Sort by"
            value={sort}
            onChange={(evt) => {
              setSortOrder(evt.target.value);
              debouncedFetch();
            }}
          >
            <MenuItem value={'{"price.sort":"ASC"}'}>
              Price: ASC
            </MenuItem>
            <MenuItem value={'{"price.sort":"DESC"}'}>
              Price: DESC
            </MenuItem>
          </TextField>
        </Stack>
      </Paper>

      {offersView === 'TABLE' ? <ListingsTable listings={selectedListings} loading={loading} /> : <ListingsGrid listings={selectedListings} loading={loading} />}
    </>
  )
}