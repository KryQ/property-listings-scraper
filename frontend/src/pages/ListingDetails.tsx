import {
	Box,
	Typography,
	Paper,
	IconButton,
	Stack,
	List,
	Button,
	Link,
} from "@mui/material";
import Grid from "@mui/material/Unstable_Grid2";
import { useEffect, useState } from "react";
import {
	ChevronLeft,
	ChevronRight,
	Favorite,
	FavoriteBorder,
} from "@mui/icons-material";
// @ts-ignore
import { useListingsStore } from "../store/listings";
import { type Diff, type ListingChange, getListingChanges } from "../api";
import { isListingFavorite, useUserStore } from "../store/user";

const ListingImage = ({
	listingId,
	images,
}: { listingId: number; images: string[] }) => {
	const [imageIndex, setImageIndex] = useState(0);

	// biome-ignore lint/correctness/useExhaustiveDependencies: <explanation>
	useEffect(() => {
		setImageIndex(0);
	}, [listingId, setImageIndex]);

	const nextImage = () => {
		if (images.length - 1 <= imageIndex) {
			setImageIndex(0);
			return;
		}

		setImageIndex(imageIndex + 1);
	};

	const prevImage = () => {
		if (0 >= imageIndex) {
			setImageIndex(images.length - 1);
			return;
		}

		setImageIndex(imageIndex - 1);
	};

	return (
		<Paper sx={{ display: "flex", overflow: "hidden", position: "relative" }}>
			<img
				loading="lazy"
				alt="ll"
				src={images[imageIndex]}
				style={{ width: "100%" }}
			/>
			<Typography
				sx={{
					position: "absolute",
					bottom: "8px",
					right: "16px",
					color: "white",
				}}
			>
				{`${imageIndex + 1}/${images.length}`}
			</Typography>
			<IconButton
				onClick={() => nextImage()}
				sx={{ position: "absolute", top: "50%", right: "16px", color: "white" }}
			>
				<ChevronRight />
			</IconButton>
			<IconButton
				onClick={() => prevImage()}
				sx={{ position: "absolute", top: "50%", left: "16px", color: "white" }}
			>
				<ChevronLeft />
			</IconButton>
		</Paper>
	);
};

const AttributeChangeDetails: React.FC<{ change: Diff }> = ({ change }) => {
	const getChangeLabel = (changeKind: "N" | "D" | "E") => {
		switch (changeKind) {
			case "D":
				return "Deleted";
			case "E":
				return "Changed";
			case "N":
				return "New";
		}
	};

	return (
		<Box sx={{ ml: 3 }}>
			<Typography component="span">{`${getChangeLabel(
				change.kind,
			)} ${change.path.join(",")} `}</Typography>
			<Typography
				component="span"
				sx={{ fontWeight: "bold" }}
			>{`${change.lhs} `}</Typography>
			<span style={{ fontWeight: "bold" }}>&#8594;</span>
			<Typography
				component="span"
				sx={{ fontWeight: "bold" }}
			>{` ${change.rhs}`}</Typography>
		</Box>
	);
};

type ListingDetailsParams = { params: { id: string } };
export const ListingDetails: React.FC<ListingDetailsParams> = ({ params }) => {
	const [changes, setChanges] = useState<ListingChange[]>([]);
	const { getListing, addToFavorites, removeFromFavorites } =
		useListingsStore();
	const isFavorite = useUserStore(
		isListingFavorite(Number.parseInt(params.id, 10)),
	);
	const listing = getListing(params.id);

	useEffect(() => {
		getListingChanges(params.id).then((changes) => {
			setChanges(changes);
		});
	}, [params.id]);

	if (!listing) {
		return <Typography variant="h2">No listing found</Typography>;
	}

	const distinctListingChanges = [
		...new Set(
			changes.map(({ changes }) => changes.map(({ path }) => path)).flat(2),
		),
	];

	return (
		<Grid spacing={1} container>
			<Grid xs={12}>
				{changes.length > 0 && (
					<Paper sx={{ p: 2 }}>
						<Typography variant="h6">Changes Summary</Typography>
						<Stack
							direction="row"
							justifyContent="space-around"
							sx={{ height: "100px" }}
						>
							{distinctListingChanges.map((change) => (
								<Typography key={change}>{change}</Typography>
							))}
						</Stack>
						<Typography variant="h6">Changes</Typography>
						<List>
							{changes.map((change) => (
								<Box
									sx={{ display: "flex", flexDirection: "column", mb: 2 }}
									key={change.id}
								>
									<Typography>{`Changes done ${change.createdAt}`}</Typography>
									{change.changes.map((attrChange, index) => (
										<AttributeChangeDetails
											key={`${change.id}-${index}`}
											change={attrChange}
										/>
									))}
								</Box>
							))}
						</List>
					</Paper>
				)}
			</Grid>
			<Grid xs={6}>
				<Link href={listing.link}>
					<Button size="large" variant="contained" fullWidth>
						Go to Listing
					</Button>
				</Link>
			</Grid>
			<Grid xs={6}>
				<Button
					size="large"
					variant="contained"
					fullWidth
					onClick={() => {
						isFavorite
							? removeFromFavorites(listing.id)
							: addToFavorites(listing.id);
					}}
					startIcon={isFavorite ? <FavoriteBorder /> : <Favorite />}
				>
					{isFavorite ? "Remove from favorites" : "Add to favorites"}
				</Button>
			</Grid>
			<Grid xs={12} lg={6}>
				<ListingImage listingId={listing.id} images={listing.images} />
			</Grid>
			<Grid xs={12} lg={6}>
				<Paper sx={{ p: 2 }}>
					<Stack direction="column">
						<Typography variant="h4">{listing.title}</Typography>
						<Typography
							variant="body2"
							// biome-ignore lint/security/noDangerouslySetInnerHtml: <explanation>
							dangerouslySetInnerHTML={{ __html: listing.shortDescription }}
						/>
					</Stack>
				</Paper>
			</Grid>
		</Grid>
	);
};
