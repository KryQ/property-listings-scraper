import {
	FormControl,
	InputLabel,
	LinearProgress,
	MenuItem,
	Paper,
	Select,
	Stack,
	Typography,
} from "@mui/material";
import { useUserStore } from "../store/user";
import {
	UserNotificationSettingEnum,
	type UserNotificationSetting,
} from "../../../common/dist/types";

export const UserSettings = () => {
	const { dirty, settings, setSettings } = useUserStore();

	return (
		<Paper sx={{ p: 2 }}>
			{dirty && <LinearProgress />}
			<Stack direction="column" spacing={2}>
				<Typography variant="h6">Ustawienia uzytkownika</Typography>

				<FormControl fullWidth>
					<InputLabel id="notifications-level">
						Listings to notify about
					</InputLabel>
					<Select
						labelId="notifications-level"
						id="notifications-level"
						value={settings?.notifications || UserNotificationSettingEnum.ALL}
						label="Listings to notify about"
						onChange={(evt) =>
							setSettings({
								notifications: evt.target.value as UserNotificationSetting,
							})
						}
					>
						<MenuItem value={UserNotificationSettingEnum.NONE}>Wcale</MenuItem>
						<MenuItem value={UserNotificationSettingEnum.FAVORITE_ONLY}>
							Tylko ulubione
						</MenuItem>
						<MenuItem value={UserNotificationSettingEnum.ALL}>
							Wszystkie
						</MenuItem>
					</Select>
				</FormControl>
			</Stack>
		</Paper>
	);
};
