import ky from 'ky';
import { isProdBuild } from './utils';
import { useUserStore } from './store/user';

import type { UserData, UserDataWithAuthToken, UserSettings } from '../../common/dist/types'

const restHost = isProdBuild ? window.location.origin : 'http://localhost:3000';
const restPath = `${restHost}/rest`;

export type Diff = {
  kind: 'N' | 'E' | 'D'
  lhs: unknown
  rhs: unknown
  path: string[]
}

export type ListingChange = {
  changes: Diff[]
  createdAt: string
  id: number
}

type Listing = {
  address: {
    city: string
    street?: string
  }
  areaSquareMeters: number | null
  changes: ListingChange
  deleted: boolean
  id: number
  images: string[]
  link: string
  listingCreatedAt: string
  price: number
  shortDescription: string
  source: 'OLX' | 'OTODOM'
  sourceId: string
  terrainSquareMeters: number | null
  title: string
  type: 'HOUSE'
}

const NotificationTypeEnum = Object.freeze({
  NEW: 'NEW',
  CHANGED: 'CHANGED',
  DELETED: 'DELETED',
} as const);
type NotificationType = (typeof NotificationTypeEnum)[keyof typeof NotificationTypeEnum];

type Notification = {
  id: number
  listing?: Listing
  listingChange?: ListingChange
  seenAt: string | null
  type: NotificationType
  createdAt: string
}

type StoreFilters = {
  price: number[],
  title: string,
}

const getClient = () => {
  const { authToken } = useUserStore.getState();

  return authToken ? ky.extend({
    hooks: {
      beforeRequest: [
        request => {
          request.headers.set('Authorization', `Bearer ${authToken}`);
        }
      ]
    }
  }) : ky;
};

export const getListings = (filters: StoreFilters, sort: Record<string, string>) => {
  const url = new URLSearchParams();
  url.set('price.gt', `${filters.price[0]}`)
  url.set('price.lt', `${filters.price[1]}`)

  const sortEntries = Object.entries(sort)
  url.set(sortEntries[0][0], sortEntries[0][1]);

  if (filters.title) {
    url.set('title.like', filters.title);
  }

  return getClient().get(`${`${restPath}/listing`}?${url.toString()}`).json<Listing[]>();
}

export const getListingChanges = (listingId: number | string) => getClient().get(`${restPath}/listing/${listingId}/changes`).json<ListingChange[]>();
export const markListingAsFavorite = (listingId: number | string) => getClient().get(`${restPath}/listing/${listingId}/favorite`);
export const removeListingFromFavorites = (listingId: number | string) => getClient().get(`${restPath}/listing/${listingId}/unfavorite`);

export const getNotifications = () => getClient().get(`${restPath}/notification?limit=50`).json<Notification[]>();
export const markNotificationsAsSeen = (id: number | string) => getClient().get(`${restPath}/notification/${id}/seen`);
export const markAllNotificationsAsSeen = () => getClient().get(`${restPath}/notification/mark-all-seen`);

export const userInit = () => getClient().get(`${restPath}/user/init`).json<UserData>();
export const userLogin = (login: string, password: string) => getClient().post(`${restPath}/user/login`, { json: { login, password } }).json<UserDataWithAuthToken>();
export const userUpdateSettings = (settings: UserSettings) => getClient().patch(`${restPath}/user/settings`, { json: settings }).json<UserSettings>()