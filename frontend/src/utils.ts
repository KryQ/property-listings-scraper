export const isProdBuild = import.meta.env.PROD

export const sleep = (ms: number) => new Promise((resolve) => setTimeout(() => resolve(true), ms));

export const chunk = (arr: unknown[], chunkSize = 1, cache: unknown[] = []) => {
  const tmp = [...arr]
  if (chunkSize <= 0) return cache
  while (tmp.length) cache.push(tmp.splice(0, chunkSize))
  return cache
}