import { create } from 'zustand';
import { getListings, markListingAsFavorite, removeListingFromFavorites } from '../api';
import { useUserStore } from './user';

export const useListingsStore = create((set, get) => ({
  listings: [],
  loading: false,
  loadingFailed: false,
  fetchListings: async () => {
    set({ loading: true });

    try {
      const listings = await getListings(get().filters, JSON.parse(get().sort));
      set({ listings, loading: false });
    }
    catch (e) {
      console.log(e);
      set({ loadingFailed: e.message });
    }
  },
  getListing: (listingId) => {
    return get().listings.find(listing => listing.id == Number(listingId));
  },
  filters: {
    price: [0, 1000000],
    title: "",
  },
  setFilters: (filters) => {
    set({ filters })
  },
  sort: '{"price.sort":"ASC"}',
  setSortOrder: (order) => set({ sort: order }),
  addToFavorites: async (listingId) => {
    try {
      await markListingAsFavorite(listingId);
      useUserStore.setState((prev) => ({ favourites: [...prev.favourites, listingId] }));
    } catch (e) {
      console.error(e);
      return;
    }
  },
  removeFromFavorites: async (listingId) => {
    try {
      await removeListingFromFavorites(listingId);
      useUserStore.setState((prev) => ({ favourites: prev.favourites.filter(listing => listing !== listingId) }));
    } catch (e) {
      console.error(e);
      return;
    }
  }
}));

