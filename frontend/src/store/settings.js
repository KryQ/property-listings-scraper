import { create } from "zustand";

export const useSettingsStore = create((set) => ({
  offersView: 'TABLE',
  changeOffersView: (view) => {
    set({ offersView: view });
  },
}))