import { create } from 'zustand';
import { getNotifications, markNotificationsAsSeen, markAllNotificationsAsSeen } from '../api';

export const useNotificationStore = create((set) => ({
  notifications: [],
  loading: false,
  loadingFailed: false,
  fetchNotifications: async () => {
    set({ loading: true });

    try {
      const notifications = await getNotifications();
      set({ notifications, loading: false });
    }
    catch (e) {
      console.log(e);
      set({ loadingFailed: e.message });
    }
  },
  markAsSeen: async (id) => {
    try {
      await markNotificationsAsSeen(id);
      set((state) => {
        const notification = state.notifications.find((notification) => notification.id === id);

        if (!notification) {
          console.warn(`No notification with ${id} found`);
          return;
        }

        notification.seenAt = new Date();

        return { notifications: state.notifications };
      });
    }
    catch (e) {
      console.error(e);
    }
  },
  markAllAsSeen: async () => {
    try {
      await markAllNotificationsAsSeen();

      set((state) => {
        for (const notification of state.notifications) {
          if (notification.seenAt) return;

          notification.seenAt = new Date();
        };

        return { notifications: state.notifications };
      });
    }
    catch (e) {
      console.error(e);
    }
  },
}));
