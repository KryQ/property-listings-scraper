import { create } from "zustand";
import { userInit, userLogin, userUpdateSettings } from "../api";
import type { UserSettings } from "../../../common/dist/types";

type UserStore = {
  dirty: boolean,
  loggingIn: boolean,
  id: number | null,
  login: string | null,
  settings: UserSettings | null,
  favourites: number[],
  authToken: string | null,

  check: () => Promise<boolean>,
  logIn: (login: string, password: string) => Promise<boolean>,
  logout: () => Promise<boolean>,
  setSettings: (settings: UserSettings) => Promise<void>
}

export const useUserStore = create<UserStore>((set): UserStore => ({
  dirty: false,
  loggingIn: false,
  id: null,
  login: null,
  settings: null,
  favourites: [],
  authToken: null,

  check: async () => {
    const jwtToken = localStorage.getItem('userJwtToken')

    if (!jwtToken) {
      return false;
    }

    set({ authToken: jwtToken });

    try {
      const user = await userInit();
      set({ id: user.id, settings: user.settings, authToken: jwtToken, favourites: user.favorites || [] });

      return true;
    } catch (e) {
      set({ authToken: null });
      return false;
    }
  },
  logIn: async (login, password) => {
    set({ loggingIn: true });

    try {
      const response = await userLogin(login, password);

      set({ id: 1, login: response.login, settings: response.settings, authToken: response.token });
      localStorage.setItem('userJwtToken', response.token);
      return true;
    } catch (e) {
      console.log(e);
      return false;
    } finally {
      set({ loggingIn: false });
    }
  },
  logout: async () => {
    set({ id: null, login: null, settings: null, authToken: null, favourites: [] });
    return true;
  },
  setSettings: async (settings: UserSettings) => {
    set({ settings, dirty: true });
    await userUpdateSettings(settings);
    set({ dirty: false })
  }
}))

export const isListingFavorite = (listingId: number) => (user: UserStore): boolean =>
  user.favourites.findIndex(
    (favorite) => favorite === listingId,
  ) >= 0