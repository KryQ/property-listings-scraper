import type { DataSource } from 'typeorm';
import type { FastifyBaseLogger } from 'fastify';
import type { Listing } from '../entities/Listing.js';

export type SearchCriteria = {
  ownerTypeSingleSelect: 'ALL'
  buildingType: '[DETACHED]'
  locations: string[]
  priceMax: number
  by: 'DEFAULT'
  direction: 'DESC' | 'ASC'
  viewType: 'listing'
  searchingCriteria: ('sprzedaz' | 'dom' | 'wiele-lokalizacji')[]
}

export type ScrapeUpdateReview = { created: number, deleted: number, updated: number }

export type ScraperInterface = (db: DataSource, logger: FastifyBaseLogger) => Promise<ScrapeUpdateReview>;

export type ListingCreationProps = Omit<Listing, 'id' | 'deleted' | 'changes'>;