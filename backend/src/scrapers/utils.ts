import type { FastifyBaseLogger } from "fastify";
import { ArrayContains, JsonContains, Not, type DataSource, type EntityManager } from "typeorm";
import type { ListingCreationProps, ScrapeUpdateReview } from "./types.js";
import { Listing, type ListingSource } from "../entities/Listing.js";
import { ListingChanges } from "../entities/ListingChanges.js";
import { Notification } from "../entities/Notification.js";
import _ from "lodash";
import DeepDiffPkg from 'deep-diff';
import { User } from "../entities/User.js";
import { UserNotificationSettingEnum } from "../../../common/dist/types.js";
import { SystemLogSourceEnum } from "../entities/SystemLog.js";
import { SystemLogController } from "../controllers/SystemLogController.js";

const { diff } = DeepDiffPkg;

const pushNotificationsToProperUsers = async (transaction: EntityManager, notification: Partial<Notification>): Promise<Notification[]> => {
  const userRepository = transaction.getRepository(User);
  const notificationRepo = transaction.getRepository(Notification);

  const users = await userRepository.find({
    where: {
      settings: Not({
        notifications: UserNotificationSettingEnum.NONE
      }),
    }
  })

  return users.map(user => notificationRepo.create({ ...notification, user }));
}

export const compareAndStoreListing = async (db: DataSource, logger: FastifyBaseLogger, offers: ListingCreationProps[], listingsSource: ListingSource): Promise<ScrapeUpdateReview> => {
  logger.debug(`Got ${offers.length} listings to process`);

  return db.manager.transaction(async (transaction: EntityManager) => {
    const listingRepo = transaction.getRepository(Listing);
    const changesRepo = transaction.getRepository(ListingChanges);
    const notificationRepo = transaction.getRepository(Notification);

    const existingListings = await listingRepo.find({
      where: { source: listingsSource, deleted: false }
    });

    const offersToCreate: Listing[] = [];
    const offersToUpdate: Listing[] = [];
    const notificationsToCreate: Notification[] = [];

    for (const offer of offers) {
      const existingListing = existingListings.find(dbListing => dbListing.sourceId === offer.sourceId);

      // Check if it's a new offer
      if (!existingListing) {
        const listing = listingRepo.create(offer)
        notificationsToCreate.push(...(await pushNotificationsToProperUsers(transaction, {
          type: 'NEW',
          changes: {
            source: listingsSource,
            sourceId: listing.sourceId,
          }
        })));

        offersToCreate.push(listing);
      }
      // Update existing offer
      else {
        const offerDiff = diff(_.omit(existingListing, ['id', 'deleted', 'listingCreatedAt']), _.omit(offer, 'listingCreatedAt'));

        if (offerDiff) {
          offersToUpdate.push({ ...existingListing, ...offer });

          const change = changesRepo.create({
            listing: existingListing,
            changes: offerDiff,
          })

          await changesRepo.save(change);

          notificationsToCreate.push(...(await pushNotificationsToProperUsers(transaction, {
            type: 'CHANGED',
            changes: {
              source: listingsSource,
              sourceId: existingListing.sourceId,
              changeId: change.id,
            }
          })));
        }

        const index = existingListings.indexOf(existingListing);
        if (index > -1) existingListings.splice(index, 1);
      }
    }

    logger.debug(`Left with ${existingListings.length} orphaned listings, Not deleting`);

    // await listingRepo.update({ id: In(existingListings.map(({ id }) => id)) }, { deleted: true });

    // for (const listing of existingListings) {
    //   notificationsToCreate.push(notificationRepo.create({
    //     type: 'DELETED',
    //     changes: {
    //       source: listingsSource,
    //       sourceId: listing.sourceId,
    //     }
    //   }));
    // }

    await listingRepo.save([...offersToCreate, ...offersToUpdate]);
    await notificationRepo.save(notificationsToCreate);


    const sysLogger = new SystemLogController(db);
    await sysLogger.info(SystemLogSourceEnum.SCRAPER, `Scrapped ${offers.length} from ${listingsSource} Updated: ${offersToUpdate.length} Created: ${offersToCreate.length}`);

    return {
      created: offersToCreate.length,
      updated: offersToUpdate.length,
      deleted: 0, //existingListings.length,
    }
  })
}