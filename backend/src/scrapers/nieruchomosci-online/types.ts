export interface Welcome {
  backSerachUrl: string;
  ModuleAgentContactLead: ModuleAgentContactLead;
  ModuleUserSearchesNotifications: ModuleUserSearchesNotifications;
  surveyType: null;
  surveyHook: string;
  surveyFlow: string;
  listPollData: ListPollData;
  primaryListPollData: null;
  listHTML: string;
  paginationHTML: string;
  jsonPhotos: JSONPhotos;
  listAdditionalData: AdDetails;
  unsortableList: number;
  listInfo: ListInfo;
  dataLayer: DataLayer;
  ModuleSearch: ModuleSearch;
  searchChecksum: string;
  agentsBox: AgentsBox;
  searchValues: SearchValues;
}

export interface ModuleAgentContactLead {
  showAgentContact: boolean;
  autoShow: boolean;
  seenForm: boolean;
}

export interface ModuleSearch {
  listHeader: string;
  listName: string;
  searchHistory: SearchHistory[];
  isSearchMetroAvailable: boolean;
  isListCriteriaApiAvailable: boolean;
}

export interface SearchHistory {
  id: string;
  cn: CityName;
  rn: RegionAlias;
  is4: string;
  idCo: string;
  idD: string;
  idR: string;
  isG: string;
  idQ: number;
  idS: number;
  pg: number;
  isSc: number;
}

export enum CityName {
  Bydgoszcz = "Bydgoszcz",
}

export enum RegionAlias {
  KujawskoPomorskie = "kujawsko-pomorskie",
}

export interface ModuleUserSearchesNotifications {
  savedEmail: null;
  userSearchesNotificationCriteria: UserSearchesNotificationCriteria;
}

export interface UserSearchesNotificationCriteria {
  isActiveNotification: boolean;
  idUserSearch: number;
  isCurrentSearchSaved: number;
  isBoxShown: number;
}

export interface AgentsBox {
  agentsExists: number;
  html: string;
}

export interface DataLayer {
  tempDl: TempDL;
  "search-parameters": SearchParameters;
}

export interface SearchParameters {
  version: null;
  categoryAliasSingle: null;
  adTypeAlias: null;
  regionAlias: RegionAlias;
  cityName: CityName;
  quarterName: null;
  streetName: null;
  distance: null;
  priceFrom: null;
  priceTo: string;
  areaFrom: null;
  areaTo: null;
  isPhoto: null;
  isPrivateAd: null;
  parking: null;
  idAd: null;
  idAccount: null;
  plotAreaFrom: null;
  plotAreaTo: null;
  pricePMFrom: null;
  pricePMTo: null;
  isPrimaryMarketAd: null;
  roomNumberFrom: null;
  roomNumberTo: null;
  isForHouseDestiny: null;
  isAsphaltDrive: null;
  isFenced: null;
  isOfficeBuilding: null;
  addedAfter: null;
  constructionYear: null;
  isUnfurnished: null;
  isForInvestmentDestiny: null;
  isEntranceFromStreet: null;
  isWarehouse: null;
  isSeparateKitchen: null;
  isGroundFloor: null;
  isOnGroundFloor: null;
  isMultiLevel: null;
  isBalcony: null;
  isElevator: null;
  floorFrom: null;
  floorTo: null;
  isBasement: null;
  isApartment: null;
  isTenementHouse: null;
  isGarage: null;
  idInvestment: null;
  withBuildingProjects: null;
  noAgentProvision: null;
  idContactPerson: null;
  idRoomCapacity: null;
  isPlaceInRoom: null;
  idPreferedProfession: null;
  isFurnished: null;
  isOwnerNotInHouse: null;
  isMInternet: null;
  isMTv: null;
  availableFrom: null;
  isInvestmentsOnly: null;
  idIsBuildingType: null;
  idIsPlotType: null;
  isPossibleChange: null;
  isTerrace: null;
  isGarden: null;
  isWithoutRent: null;
  isForRenovation: null;
  isSubway: null;
  isStudio: null;
  isDetached: number;
  isLake: null;
  isWaterside: null;
  isArmed: null;
  isWithTenant: null;
  isForGastronomic: null;
  isForWarehouse: null;
  isSecondaryMarketAd: null;
  isOnlinePresentationAvailable: null;
}

export interface TempDL {
  adIds: string[];
  investmentAdIds: unknown[];
  investmentIds: unknown[];
}

export interface JSONPhotos {
  [key: string]: A23263323Class;
}

export interface A23263323Class {
  c: A21432514C[];
  l: string[];
  x: string[];
}

export interface A21432514C {
  idPhoto: string;
  alt: Alt;
  url: string;
  ip: boolean;
  pc: string;
  fitType: FitType;
}

export enum Alt {
  DOMBalkon = "Dom balkon",
  DOMBydgoszcz = "Dom Bydgoszcz",
  DOMBydgoszczSprzedaż = "Dom Bydgoszcz sprzedaż",
  DOMTaras = "Dom taras",
  DOMWolnostojącySprzedaż = "Dom wolnostojący sprzedaż",
  DOMZBalkonem = "Dom z balkonem",
  DOMZGarażem = "Dom z garażem",
  DOMZParkingiem = "Dom z parkingiem",
  DziałkaBydgoszcz = "Działka Bydgoszcz",
  DziałkaLubiczDolny = "Działka Lubicz Dolny",
  DziałkaRekreacyjnaBydgoszcz = "Działka rekreacyjna Bydgoszcz",
  DziałkaRekreacyjnaBydgoszczSprzedam = "Działka rekreacyjna Bydgoszcz sprzedam",
  DziałkaRekreacyjnaSprzedam = "Działka rekreacyjna sprzedam",
}

export enum FitType {
  Fill = "fill",
}

export interface A22269809 {
  c: A22269809C[];
  l: string[];
  x: string[];
}

export interface A22269809C {
  idPhoto: number;
  alt: string;
  url: string;
  ip: boolean;
  pc: string;
  fitType: FitType;
}

export interface AdDetails {
  isSupplementAd: boolean;
  map?: Map;
  attributes: Attribute[];
  contactBox: ContactBox;
  modDate: string;
  changeDate: string;
  idAccountType: string;
  isAccountActive: boolean;
  isIbo: number;
  isNoProvision: number;
  idAdType: string;
  primaryPrice: string;
  idCategory: string;
  metaTitle: string;
  h1: string;
  h2: string;
  isArchive: string;
  asArchive: number;
  boxText: string;
  video: string;
  isOpenDay: number;
  openDayDate: Date | string;
  openDayDateFull: Date | string | null;
  shareUrl: string;
  thermometer: number;
  isHot: number;
  isOneAgencyOnly: number;
  virtualTour: string;
  virtualTourType: number;
  virtualTourShowType: number;
  isGalleryVirtualTour: boolean;
  virtualTourInfoCookies: boolean;
  isFav: number;
  notes: null;
  events: unknown[];
  adContactDefaultMessage: AdContactDefaultMessage;
  idAccount: string;
  idAA: null;
  rodoTermsParagraph: string;
  dlData: DLData;
  pt: number;
}

export enum AdContactDefaultMessage {
  JestemZainteresowanyOgłoszeniemProszęOKontakt = "Jestem zainteresowany ogłoszeniem, proszę o kontakt.",
}

export interface Attribute {
  label: string;
  alias: string;
  values?: Value[];
}

export interface Value {
  value: string;
  type: ValueType;
}

export enum ValueType {
  BigNumber = "big-number",
  SmallText = "small-text",
  TextDash = "text-dash",
}

export interface ContactBox {
  dl: DL;
  personName: string;
  photo: string;
  photoMini: string;
  photosAgentForMini: string;
  personValidate: string;
  nameShow: string;
  photoBlank: string;
  isProfessional: number;
  companyName: string;
  phoneHDots: string;
  phoneH: string;
  phone2H: string;
  accType: CType;
  adsUrl: AdsURL;
}

export enum CType {
  Agency = "agency",
  Agent = "agent",
}

export interface AdsURL {
  type: AdsURLType;
  url: string;
  alike: string;
  anchor: Anchor;
  at: At;
  cType: CType;
}

export enum Anchor {
  PełnaOfertaAgenta = "Pełna oferta agenta",
  PełnaOfertaBiura = "Pełna oferta biura",
}

export enum At {
  Agenta = "Agenta",
  Biura = "Biura",
}

export enum AdsURLType {
  Alike = "alike",
  Normal = "normal",
}

export interface DL {
  category: Category;
  type: DLType;
  populationGroup: string;
  isArchive: string;
}

export enum Category {
  Domy = "Domy",
}

export enum DLType {
  Sprzedaż = "sprzedaż",
}

export interface DLData {
  cityName: CityName;
  regionName: RegionAlias;
  quarterName: null;
  idAdType: string;
  primaryPrice: string;
  idCategory: string;
  idRecord: string;
  investmentId: string;
}

export interface PoisClass {
  icon: Icon;
  points: Point[];
}

export interface Icon {
  view: string;
  hover: string;
}

export interface Point {
  label: string;
  latitude: string;
  longitude: string;
}

export interface Map {
  latitude: string;
  longitude: string;
  accuracy: string;
  isInaccurateLocation: number;
  mrk: string;
  pois: PoisClass;
  inaccurateLocation?: number;
}

export interface ListInfo {
  isSupplementOnlyResult: boolean;
  showSupplements: boolean;
  hideOrderBox: boolean;
  totalAdsFoundCounter: number;
  showMap: boolean;
  totalAdsFoundCounterVisible: boolean;
}

export interface ListPollData {
  imgPath: string;
  data: Data;
  dictionary: Dictionary;
  surveyType: string;
  rodo: Rodo;
  script: Script;
}

export interface Data {
  isPollAvailable: boolean;
  idCategory: number;
  idAdType: number;
  wasPollClosed: boolean;
  wasPollFinished: boolean;
  isPollVisible: boolean;
}

export interface Dictionary {
  idCategory: AreaFrom[];
  idAdType: AreaFrom[];
  purpose: AreaFrom[];
  idMarketType: AreaFrom[];
  roomNumberFrom: AreaFrom[];
  areaFrom: AreaFrom[];
  price: AreaFrom[];
}

export interface AreaFrom {
  name: string;
  type: AreaFromType;
  options: Option[];
  idCategories: number[];
  idAdTypes: number[];
}

export interface Option {
  label: string;
  value: string;
  name: string;
}

export enum AreaFromType {
  Select = "select",
}

export interface Rodo {
  url: string;
  text: string;
  urlText: string;
  idFormSource: number;
  debugId: number;
}

export interface Script {
  wrapperHtml: string;
  pollScript: string;
}

export interface SearchValues {
  idCategory: string;
  idAdType: string;
  cityNameVariant_2: string;
  cityName: CityName;
  idCity: number;
  idRegion: string;
  location: Location;
  priceTo: string;
  isDetached: number;
}

export interface Location {
  locationType: string;
  citiesIds: string[];
  districtsIds: string[];
  regionsIds: string[];
  idRegion: string;
}
