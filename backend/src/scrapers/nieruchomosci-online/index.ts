import type { DataSource } from "typeorm";
import type { FastifyBaseLogger } from "fastify";
import * as cheerio from 'cheerio';

import type { ListingCreationProps, ScraperInterface } from "../types.js";
import type { AdDetails, Welcome } from "./types.js";
import { ListingSourceEnum } from "../../entities/Listing.js";
import { compareAndStoreListing } from "../utils.js";

let logger!: FastifyBaseLogger;

type RequestHashProps = {
  num: number // TODO: Explain this variable
  propertyType: 'dom',
  listingType: 'sprzedaz',
  location: string
  price: {
    max: number,
  },
}
const composeRequestHash = (props: RequestHashProps) => {
  return [
    props.num,
    props.propertyType,
    props.listingType,
    null,
    props.location,
    null,
    null,
    null,
    `-${props.price.max}`,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    1,
  ].join(',')
}

interface AdDetailsExpanded extends AdDetails {
  sourceId: string;
  images: string[];
  area: number | null;
  terrainArea: number | null;
}

const fetchListings = async (host: string, page = 1): Promise<AdDetailsExpanded[]> => {
  const qs = new URLSearchParams();
  if (page > 1) {
    qs.set('p', `${page}`);
  }

  const composedRequestPath = `${host}&${qs.toString()}`;
  logger.debug(`Asking ${composedRequestPath} for listings`);
  const request = await fetch(composedRequestPath, {
    headers: {
      "x-requested-with": "XMLHttpRequest",
    },
    method: "GET"
  });
  const body = await request.json() as Welcome;

  const $ = cheerio.load(body.listHTML);

  return Object
    .entries(body.listAdditionalData)
    .map(([key, value]) => {
      const images = body.jsonPhotos[key];
      const buildingArea = $(`div[data-id="${key}"] span[id^="area-display_"]`).text();
      const terrainArea = $(`div[data-id="${key}"] tbody > tr > td:nth-child(3) > span`).text();
      return {
        sourceId: key,
        area: Number.parseFloat(buildingArea.replace(/[^\d.,]/g, "").replace(',', '.')) || null,
        terrainArea: Number.parseFloat(terrainArea.replace(/[^\d.,]/g, "").replace(',', '.')) || null,
        images: images?.x,
        ...value
      }
    })
    .filter(listing => !listing.isSupplementAd); // Filter out ads
}

const fetchAllListings = async (params: RequestHashProps): Promise<AdDetailsExpanded[]> => {
  const host = `https://www.nieruchomosci-online.pl/szukaj.html?${composeRequestHash(params)}`;

  const conuntRequest = await fetch(`${host}&q=&cntOnly`, {
    headers: {
      "x-requested-with": "XMLHttpRequest",
    },
    method: "GET"
  });
  const countBody = await conuntRequest.json() as { adsFoundCounter: number, counterText: string, mobileCriteriaCounter: number };

  logger.info(`Found ${countBody.adsFoundCounter} listings matching criteria. Fetching...`);

  let listings = await fetchListings(host);
  let page = 1;

  while (listings.length < countBody.adsFoundCounter) {
    page++;
    listings = [...listings, ...(await fetchListings(host, page))]
  }



  return listings;
}

const normalizeListing = (listing: AdDetailsExpanded): ListingCreationProps => {
  const splittedAddress = listing.h2.split(', ');
  return ({
    title: listing.h1,
    shortDescription: listing.metaTitle,
    link: listing.shareUrl,
    listingCreatedAt: new Date(), // Looks like we cant scrape creation date from API. Lets at least have it as firs found in system.
    source: ListingSourceEnum.NIERUCHOMOSCI_ONLINE,
    sourceId: listing.sourceId,
    price: Number(listing.primaryPrice),
    type: 'HOUSE',
    address: {
      city: splittedAddress.at(-2) || "Bydgoszcz",
      ...(splittedAddress.at(-3) ? { street: splittedAddress.at(-3) } : {})
    },
    images: listing.images,
    areaSquareMeters: listing.area,
    terrainSquareMeters: listing.terrainArea,
  })
}

export const scrape: ScraperInterface = async (db: DataSource, _logger: FastifyBaseLogger) => {
  logger = _logger;

  const listings = await fetchAllListings({
    listingType: 'sprzedaz',
    location: 'Bydgoszcz:28936',
    price: { max: 800000 },
    num: 3,
    propertyType: 'dom',
  });

  const normalizedListings = listings.map(normalizeListing);

  logger.info(`Fetched ${listings.length} listings`);

  return compareAndStoreListing(db, logger, normalizedListings, ListingSourceEnum.NIERUCHOMOSCI_ONLINE);
}