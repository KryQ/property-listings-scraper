import type { FastifyBaseLogger } from "fastify";
import type { DataSource } from "typeorm";
import type { Daum, Root } from "./OlxResponseType.js";
import type { ListingCreationProps, ScrapeUpdateReview } from "../types.js";
import { ListingSourceEnum } from "../../entities/Listing.js";
import _ from 'lodash';
import type { ScraperInterface } from "../types.js";
import { compareAndStoreListing } from "../utils.js";

let logger!: FastifyBaseLogger;

const fetchAllListings = async (nextPage = undefined): Promise<Daum[]> => {
  const baseAddress = 'https://www.olx.pl/api/v1/offers';
  const qs = new URLSearchParams();

  qs.set('offset', `${0}`);
  qs.set('limit', `${40}`);
  qs.set('category_id', `${18}`);
  qs.set('region_id', `${15}`);
  qs.set('city_id', `${4019}`);
  qs.set('filter_enum_builttype[0]', 'wolnostojacy');
  qs.set('filter_float_price:to', `${800000}`);

  const requestPath = nextPage ? nextPage : `${baseAddress}?${qs.toString()}`;
  logger.debug(`Asking ${requestPath} for listings`);

  const request = await fetch(requestPath);

  const response = await request.json() as Root;
  const items: Daum[] = [...response.data];

  if (response.links?.next?.href !== undefined) {
    //@ts-ignore
    const nextPage = await fetchAllListings(response.links.next.href);

    items.push(...nextPage);
  }

  return items;
}

const normalizeListing = (offer: Daum): ListingCreationProps => ({
  source: ListingSourceEnum.OLX,
  sourceId: `${offer.id}`,
  address: {
    city: offer.location.city.name,
  },
  areaSquareMeters: Number(offer.params.find(param => param.key === 'm')?.value.key) || null,
  terrainSquareMeters: Number(offer.params.find(param => param.key === 'area')?.value.key) || null,
  listingCreatedAt: new Date(offer.created_time),
  price: Number(offer.params.find(param => param.key === 'price')?.value.value),
  title: offer.title,
  type: "HOUSE", // TODO: should cast offer.estate to type enum.
  shortDescription: offer.description,
  link: offer.url,
  images: offer.photos.map(image => image.link.replace('{width}', '1920').replace('{height}', '1080')),
});

export const scrape: ScraperInterface = async (db: DataSource, _logger: FastifyBaseLogger): Promise<ScrapeUpdateReview> => {
  logger = _logger;

  const listings = (await fetchAllListings()).map(normalizeListing);

  return compareAndStoreListing(db, logger, listings, ListingSourceEnum.OLX);
}