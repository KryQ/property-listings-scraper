export interface Root {
  data: Daum[]
  metadata: Metadata
  links: Links
}

export interface Daum {
  id: number
  url: string
  title: string
  last_refresh_time: string
  created_time: string
  valid_to_time: string
  pushup_time?: string
  description: string
  promotion: Promotion
  params: Param[]
  key_params: string[]
  business: boolean
  user: User
  status: string
  contact: Contact
  map: Map
  location: Location
  photos: Photo[]
  partner?: Partner
  category: Category
  delivery: Delivery
  safedeal: Safedeal
  shop: Shop
  offer_type: string
  external_url?: string
  omnibus_pushup_time?: string
}

export interface Promotion {
  highlighted: boolean
  urgent: boolean
  top_ad: boolean
  options: string[]
  b2c_ad_page: boolean
  premium_ad_page: boolean
}

export interface Param {
  key: string
  name: string
  type: string
  value: Value
}

export interface Value {
  key?: string
  label: string
  value?: number
  type?: string
  arranged?: boolean
  budget?: boolean
  currency?: string
  negotiable?: boolean
  converted_value: null
  previous_value?: number
  converted_previous_value: null
  converted_currency: null
  previous_label?: string
}

export interface User {
  id: number
  created: string
  other_ads_enabled: boolean
  name: string
  logo: null
  logo_ad_page?: string
  social_network_account_type?: string
  photo: null
  banner_mobile: string
  banner_desktop: string
  company_name: string
  about: string
  b2c_business_page: boolean
  is_online: boolean
  last_seen: string
  seller_type: null
  uuid: string
}

export interface Contact {
  name: string
  phone: boolean
  chat: boolean
  negotiation: boolean
  courier: boolean
}

export interface Map {
  zoom: number
  lat: number
  lon: number
  radius: number
  show_detailed: boolean
}

export interface Location {
  city: City
  region: Region
}

export interface City {
  id: number
  name: string
  normalized_name: string
}

export interface Region {
  id: number
  name: string
  normalized_name: string
}

export interface Photo {
  id: number
  filename: string
  rotation: number
  width: number
  height: number
  link: string
}

export interface Partner {
  code: string
}

export interface Category {
  id: number
  type: string
}

export interface Delivery {
  rock: Rock
}

export interface Rock {
  offer_id: null
  active: boolean
  mode: string
}

export interface Safedeal {
  weight: number
  weight_grams: number
  status: string
  safedeal_blocked: boolean
  allowed_quantity: null[]
}

export interface Shop {
  subdomain: null
}

export interface Metadata {
  total_elements: number
  visible_total_count: number
  promoted: number[]
  search_id: string
  adverts: Adverts
  search_suggestion: SearchSuggestion
  source: Source
}

export interface Adverts {
  places: null
  config: Config
}

export interface Config {
  targeting: Targeting
}

export interface Targeting {
  env: string
  lang: string
  account: string
  dfp_user_id: string
  user_status: string
  cat_l0_id: string
  cat_l1_id: string
  cat_l2_id: string
  cat_l0: string
  cat_l0_path: string
  cat_l1: string
  cat_l1_path: string
  cat_l2: string
  cat_l2_path: string
  cat_l0_name: string
  cat_l1_name: string
  cat_l2_name: string
  cat_id: string
  private_business: string
  offer_seek: string
  view: string
  region_id: string
  region: string
  subregion: string
  city_id: string
  city: string
  search_engine_input: string
  page: string
  segment: null[]
  app_version: string
  builttype: string[]
  price: string[]
}

export interface SearchSuggestion {
  type: string
  url: string
  changes: Changes
}

export interface Changes {
  strategy: string
}

export interface Source {
  promoted: number[]
  organic: number[]
}

export interface Links {
  self: Link
  first: Link
  next?: Link
}

export interface Link {
  href: string
}

