import "reflect-metadata"
import _ from 'lodash';
import type { FastifyBaseLogger } from "fastify";
import type { DataSource } from "typeorm";

import type { ListingCreationProps, ScraperInterface } from '../types.js'
import type { OtoDomSearchResponse, Item, Pagination } from "./OtodomResponseType.js";
import { ListingSourceEnum } from "../../entities/Listing.js"
import type { SearchCriteria } from "../types.js";
import { compareAndStoreListing } from "../utils.js";

let logger!: FastifyBaseLogger;

type RequestParams = {
  path: string
  body?: string | object
}

type SuccesfullResponse<T> = { response: T }
type FailedResponse = { error: string }

const request = async <T>(params: RequestParams): Promise<SuccesfullResponse<T> | FailedResponse> => {
  try {
    const request = await fetch(params.path);

    if (request.status < 200 && request.status > 299) {
      return Promise.resolve({ error: `${request.status}` })
    }

    return { response: (await request.json()) as T }
  } catch (e) {
    return Promise.resolve({ error: (e as Error).message })
  }
}

const isFailedRequest = (response: unknown): response is FailedResponse => {
  if ((response as FailedResponse).error) {
    return true;
  }

  return false;
}

type FetchListingSuccessfullResponse = { items: Item[], pagination: Pagination };

const fetchListings = async (search: SearchCriteria, page = 1): Promise<FetchListingSuccessfullResponse | FailedResponse> => {
  // TODO: This id should be dynamically getted as it looks like it changes over time
  const queryPath = 'https://www.otodom.pl/_next/data/0Mi7cu_nUMgzgSUWOHg8r/pl/wyniki/sprzedaz/dom/wiele-lokalizacji.json';
  const qs = new URLSearchParams();

  for (const [key, value] of Object.entries(search)) {
    if (key === 'locations') {
      qs.set(key, `[${(value as string[]).join(',')}]`)
      continue;
    }

    if (typeof value === 'string') {
      qs.set(key, value)
      continue;
    }

    if (typeof value === "number") {
      qs.set(key, `${value}`)
      continue;
    }

    if (Array.isArray(value)) {
      for (const item of value) {
        qs.append(key, item)
      }
      continue;
    }

    console.error('Unhandled qs value', key, value)
  }

  if (page > 1) {
    qs.set('page', `${page}`);
  }

  const path = `${queryPath}?${qs.toString()}`;
  logger.debug(`Fetching page ${page} from: ${path}`);
  const query = await request<OtoDomSearchResponse>({ path });

  if (isFailedRequest(query)) {
    logger.error("Could not fetch listings!", query.error);
    return { error: "CANT_FETCH" };
  }

  const firstPage = query.response;
  const rawListings = firstPage.pageProps.data.searchAds.items;
  const resultPagination = firstPage.pageProps.data.searchAds.pagination;

  logger.debug(`Got ${rawListings.length} listings out of ${resultPagination.totalResults}`);

  return {
    items: rawListings,
    pagination: resultPagination,
  }
}

const fetchAllListings = async (search: SearchCriteria) => {
  const firstPage = await fetchListings(search);

  if (isFailedRequest(firstPage)) {
    logger.error('Could not fetch listings');
    return [];
  }

  const results = [...firstPage.items];

  if (firstPage.pagination.totalPages > 1) {
    const remainingPages = await Promise.all(Array
      .from<number>({ length: firstPage.pagination.totalPages - 1 })
      .map((_, page) => fetchListings(search, page + 2)))

    for (const page of remainingPages) {
      if (isFailedRequest(page)) {
        logger.error('Could not fetch listings');
        return;
      }

      results.push(...page.items);
    }
  }

  return results;
}

const normalizeListing = (offer: Item): ListingCreationProps => ({
  source: ListingSourceEnum.OTODOM,
  sourceId: `${offer.id}`,
  address: {
    city: offer.location.address.city.name,
    street: `${offer.location.address.street?.name || ""} ${offer.location.address.street?.number || ""}`.trim()
  },
  areaSquareMeters: offer.areaInSquareMeters || null,
  terrainSquareMeters: offer.terrainAreaInSquareMeters || null,
  listingCreatedAt: new Date(offer.dateCreatedFirst),
  price: offer.totalPrice.value,
  title: offer.title,
  type: "HOUSE", // TODO: should cast offer.estate to type enum.
  shortDescription: offer.shortDescription,
  link: `https://www.otodom.pl/pl/oferta/${offer.slug}`,
  images: offer.images.map(image => image.large),
});

const scrape: ScraperInterface = async (db: DataSource, _logger: FastifyBaseLogger) => {
  logger = _logger;

  logger.info("Starting Otodom scraper");

  const searchCriteria: SearchCriteria = {
    ownerTypeSingleSelect: 'ALL',
    buildingType: '[DETACHED]',
    locations: ['kujawsko--pomorskie/bydgoszcz/bydgoszcz/bydgoszcz', 'kujawsko--pomorskie/bydgoski/biale-blota'],
    priceMax: 800000,
    by: 'DEFAULT',
    direction: 'DESC',
    viewType: 'listing',
    searchingCriteria: ['dom', 'sprzedaz', 'wiele-lokalizacji'],
  }

  logger.info("Searching");
  const listings = (await fetchAllListings(searchCriteria))?.map(normalizeListing);

  if (!listings) {
    logger.error(`Couldn't get listings!`);
    throw new Error('Cant get listings')
  }

  logger.info(`Found ${listings.length} listings to compare`);

  return compareAndStoreListing(db, logger, listings, ListingSourceEnum.OTODOM)
};

export { scrape };