export interface OtoDomSearchResponse {
  pageProps: PageProps
  __N_SSP: boolean
}

export interface PageProps {
  lang: string
  _sentryTraceData: string
  _sentryBaggage: string
  pageTitle: string
  pageHeading: string
  pageDescription: string
  tracking: Tracking
  targeting: Targeting
  breadcrumbItems: BreadcrumbItem[]
  data: Data
  mapBoundingBox: MapBoundingBox
  urlViewType: string
  transaction: string
  estate: string
  location: string
  market: string
  filteringQueryParams: FilteringQueryParams
  filterLocations: FilterLocations
  filterAttributes: FilterAttributes
  sortingOption: SortingOption
  canonicalURL: string
  fieldsMetadataExperimentsVariants: any[]
  shouldEnableIndexingByMetaRobots: boolean
  signedUrlMapPreview: string
  currentUser: CurrentUser
  featureFlags: FeatureFlags
  experiments: Record<string, string>
  laquesisResult: string
  userSessionId: string
  unconfirmedUserAuthToken: any
}

export interface Tracking {
  listing: Listing
  search: Search
}

export interface Listing {
  cat_l1_id: number
  cat_l1_name: string
  business: string
  page_nb: number
  page_count: number
  result_count: number
  results_per_page: number
  listing_format: string
  ad_impressions: number[]
  selected_locations_id: string[]
  market: string
  order_by: string
  re_seller_type: string
}

export interface Search {
  ad_id_selected: any
  ad_inserted_since: any
  building_material: any
  building_type: string[]
  bungalow: any
  commodity: any
  condition: any
  distance_filt: any
  divisioning_type: any
  exclusive: any
  floor: any
  free_from: any
  from_construction_year: any
  from_height: any
  from_land_area: any
  from_nb_floors: any
  from_price_m2: any
  from_price: any
  from_surface: any
  heating: any
  infrastructure: any
  keyword: any
  main: any
  media: any
  nb_people: any
  nb_rooms: any
  non_smoker: any
  only_open_day: any
  only_private: any
  only_recreation: any
  plot_type: any
  re_seller_type: string
  remote_services: any
  rent_to_students: any
  roof_type: any
  security: any
  selected_locations_id: string[]
  structure: any
  surroundings: any
  to_construction_year: any
  to_height: any
  to_land_area: any
  to_nb_floors: any
  to_price_m2: any
  to_price: number
  to_surface: any
  use_types: any
  vicinity_types: any
  wc_count: any
  with_3d_view: any
  with_ac: any
  with_balcony: any
  with_cellar: any
  with_garage: any
  with_garden: any
  with_lift: any
  with_maisonette: any
  with_movie: any
  with_parking: any
  with_photo: any
  with_separate_kitchen: any
  with_storage_room: any
  with_terrace: any
  cat_l1_name: string
  cat_l1_id: number
  business: string
  market: string
}

export interface Targeting {
  env: string
  Country: string
  ProperType: string
  OfferType: string
  Building_type: string[]
  City: string[]
  Market: string
  PriceTo: string
  Region: string[]
  RegularUser: string
  Subregion: string[]
}

export interface BreadcrumbItem {
  id: string
  label: string
  url: string
}

export interface Data {
  searchAds: SearchAds
  searchMapPins: SearchMapPins
  searchAdsRandomPromoted: SearchAdsRandomPromoted
  searchAdsRandomInvestments: SearchAdsRandomInvestments
  searchLinksRelatedLocations: SearchLinksRelatedLocations
}

export interface SearchAds {
  __typename: string
  geometries: string[]
  locationsObjects: LocationsObject[]
  recommendationLink: any
  items: Item[]
  trackingSet: TrackingSet
  pagination: Pagination
  locationString: string
}

export interface LocationsObject {
  id: string
  detailedLevel: string
  name: string
  fullName: string
  parentIds: string[]
  geometry: Geometry
  parents: Parent[]
  children: Children[]
  __typename: string
}

export interface Geometry {
  geometry: Geometry2
  properties: any
  type: string
}

export interface Geometry2 {
  coordinates: number[][][]
  type: string
}

export interface Parent {
  id: string
  detailedLevel: string
  name: string
  fullName: string
  parentIds: string[]
  __typename: string
}

export interface Children {
  id: string
  __typename: string
}

export interface Item {
  id: number
  title: string
  slug: string
  estate: string
  developmentId: number
  developmentTitle: string
  developmentUrl: string
  transaction: string
  location: Location
  images: Image[]
  isExclusiveOffer: boolean
  isPrivateOwner: boolean
  isPromoted: boolean
  agency?: Agency
  openDays: string
  totalPrice: TotalPrice
  rentPrice?: RentPrice
  priceFromPerSquareMeter: any
  pricePerSquareMeter: PricePerSquareMeter
  areaInSquareMeters: number
  terrainAreaInSquareMeters: number
  roomsNumber: string
  hidePrice: boolean
  floorNumber: any
  investmentState: any
  investmentUnitsAreaInSquareMeters: any
  peoplePerRoom: any
  dateCreated: string
  dateCreatedFirst: string
  investmentUnitsNumber: any
  investmentUnitsRoomsNumber: any
  investmentEstimatedDelivery: any
  pushedUpAt?: string
  specialOffer: any
  shortDescription: string
  __typename: string
  totalPossibleImages: number
}

export interface Location {
  mapDetails: MapDetails
  address: Address
  reverseGeocoding: ReverseGeocoding
  __typename: string
}

export interface MapDetails {
  radius: number
  __typename: string
}

export interface Address {
  street?: Street
  city: City
  province: Province
  __typename: string
}

export interface Street {
  name: string
  number: string
  __typename: string
}

export interface City {
  name: string
  __typename: string
}

export interface Province {
  name: string
  __typename: string
}

export interface ReverseGeocoding {
  locations: Location2[]
  __typename: string
}

export interface Location2 {
  id: string
  fullName: string
  __typename: string
}

export interface Image {
  medium: string
  large: string
  __typename: string
}

export interface Agency {
  id: number
  name: string
  slug: string
  imageUrl?: string
  type: string
  brandingVisible: boolean
  highlightedAds: boolean
  __typename: string
}

export interface TotalPrice {
  value: number
  currency: string
  __typename: string
}

export interface RentPrice {
  value: number
  currency: string
  __typename: string
}

export interface PricePerSquareMeter {
  value: number
  currency: string
  __typename: string
}

export interface TrackingSet {
  cat_l1_id: number
  cat_l1_name: string
  business: string
  selected_street_id: string
  selected_district_id: string
  selected_city_id: string
  selected_subregion_id: string
  selected_region_id: string
}

export interface Pagination {
  totalResults: number
  itemsPerPage: number
  page: number
  totalPages: number
  __typename: string
}

export interface SearchMapPins {
  __typename: string
  boundingBox: BoundingBox
}

export interface BoundingBox {
  neLat: number
  neLng: number
  swLat: number
  swLng: number
  __typename: string
}

export interface SearchAdsRandomPromoted {
  __typename: string
  items: Item2[]
}

export interface Item2 {
  id: number
  title: string
  slug: string
  estate: string
  developmentId: number
  developmentTitle: string
  developmentUrl: string
  transaction: string
  location: Location3
  images: Image2[]
  isExclusiveOffer: boolean
  isPrivateOwner: boolean
  isPromoted: boolean
  agency?: Agency2
  openDays: string
  totalPrice: TotalPrice2
  rentPrice: any
  priceFromPerSquareMeter: any
  pricePerSquareMeter: PricePerSquareMeter2
  areaInSquareMeters: number
  terrainAreaInSquareMeters: number
  roomsNumber: string
  hidePrice: boolean
  floorNumber: any
  investmentState: any
  investmentUnitsAreaInSquareMeters: any
  peoplePerRoom: any
  dateCreated: string
  dateCreatedFirst: string
  investmentUnitsNumber: any
  investmentUnitsRoomsNumber: any
  investmentEstimatedDelivery: any
  pushedUpAt: string
  specialOffer: any
  shortDescription: string
  __typename: string
  totalPossibleImages: number
}

export interface Location3 {
  mapDetails: MapDetails2
  address: Address2
  reverseGeocoding: ReverseGeocoding2
  __typename: string
}

export interface MapDetails2 {
  radius: number
  __typename: string
}

export interface Address2 {
  street: any
  city: City2
  province: Province2
  __typename: string
}

export interface City2 {
  name: string
  __typename: string
}

export interface Province2 {
  name: string
  __typename: string
}

export interface ReverseGeocoding2 {
  locations: Location4[]
  __typename: string
}

export interface Location4 {
  id: string
  fullName: string
  __typename: string
}

export interface Image2 {
  medium: string
  large: string
  __typename: string
}

export interface Agency2 {
  id: number
  name: string
  slug: string
  imageUrl: string
  type: string
  brandingVisible: boolean
  highlightedAds: boolean
  __typename: string
}

export interface TotalPrice2 {
  value: number
  currency: string
  __typename: string
}

export interface PricePerSquareMeter2 {
  value: number
  currency: string
  __typename: string
}

export interface SearchAdsRandomInvestments {
  __typename: string
  items: Item3[]
}

export interface Item3 {
  id: string
  images: string[]
  price?: string
  location: string
  title: string
  hidePrice: any
  businessName: string
  businessUserDetails: BusinessUserDetails
  slug: string
  state: string
  specialOffer: any
  __typename: string
}

export interface BusinessUserDetails {
  id: number
  imageUrl?: string
  name: string
  type: string
  brandingVisible: boolean
  __typename: string
}

export interface SearchLinksRelatedLocations {
  items: Item4[]
  __typename: string
}

export interface Item4 {
  name: string
  estate: string
  transaction: string
  location: string
  count: any
  __typename: string
}

export interface MapBoundingBox {
  __typename: string
  boundingBox: BoundingBox2
}

export interface BoundingBox2 {
  neLat: number
  neLng: number
  swLat: number
  swLng: number
  __typename: string
}

export interface FilteringQueryParams {
  page: number
  limit: number
  market: string
  ownerTypeSingleSelect: string
  priceMax: number
  buildingType: string[]
  locations: Location5[]
}

export interface Location5 {
  id: string
  detailedLevel: string
  name: string
  fullName: string
  parentIds: string[]
  geometry: Geometry3
  parents: Parent2[]
  children: Children2[]
  __typename: string
}

export interface Geometry3 {
  geometry: Geometry4
  properties: any
  type: string
}

export interface Geometry4 {
  coordinates: number[][][]
  type: string
}

export interface Parent2 {
  id: string
  detailedLevel: string
  name: string
  fullName: string
  parentIds: string[]
  __typename: string
}

export interface Children2 {
  id: string
  __typename: string
}

export interface FilterLocations {
  byDomainId: ByDomainId[]
}

export interface ByDomainId {
  domainId: string
}

export interface FilterAttributes {
  market: string
  ownerTypeSingleSelect: string
  priceMax: number
  buildingType: string[]
  estate: string
  transaction: string
}

export interface SortingOption {
  by: string
  direction: string
}

export interface CurrentUser {
  id: number
  email: string
  name: string
  userType: string
  isSubAccount: boolean
  isSuspended: boolean
  isMonetizationMigrated: boolean
  isUnconfirmed: boolean
  __typename: string
}

export interface FeatureFlags {
  isAdAvmModuleEnabled: boolean
  isAdFinanceBannerEnabled: boolean
  isAdFinanceLinkEnabled: boolean
  isAdMortgageSimulatorEnabled: boolean
  isBulkSchedulingEnabled: boolean
  isFeaturedDevelopmentVASEnabled: boolean
  isListingRentPriceEnabled: boolean
  isNewApartmentsForSalePostingFormEnabled: boolean
  isNewPromotePageEnabled: boolean
  isOldSaveSearchQueryEnabled: boolean
  isOlxAdvertPromotionEnabled: boolean
  isProjectREDEnabled: boolean
  isVasRecommendationsEnabled: boolean
  isVasSchedulingEnabled: boolean
  shouldUseNewInformation: boolean
  shouldUseNewProjectPage: boolean
  isFinanceCheckboxEnabled: boolean
  isObservedAdsPageEnabled: boolean
  isUnifiedBusinessInvoicesPageActive: boolean
  isRegularMyAccountAdsPageEnabled: boolean
  isBikPromotionEnabled: boolean
  isSpecialOfferForUnitsExperimentEnabled: boolean
  isSpecialOfferEnabled: boolean
  isBulkOlxPromotionEnabled: boolean
  isRegularVasRecommendationsEnabled: boolean
  isSeoAdsDescriptionEnabled: boolean
  isPropertyEvaluationPageActive: boolean
  isSettingsContactsTabMigrated: boolean
  isVirtualAssistantEnabled: boolean
  isRegularNmfEnabled: boolean
  isExtendedBillingDataEnabled: boolean
  isNexusAdPageEnabled: boolean
  isRESellerLeadsActive: boolean
  isBadgesFeatureEnabled: boolean
  isNexusHomePageEnabled: boolean
  isPostPaymentOptionEnabled: boolean
  isStoriaTraiSectionEnabled: boolean
  isOtodomAnalyticsSectionEnabled: boolean
  __typename: string
}

