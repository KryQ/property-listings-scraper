import { Entity, PrimaryGeneratedColumn, Column, Index, ManyToOne, Relation } from "typeorm"
import { Listing } from "./Listing.js";

@Entity()
export class ListingChanges {
    @PrimaryGeneratedColumn()
    id!: number

    @ManyToOne(() => Listing, (listing) => listing.changes)
    listing!: Relation<Listing>

    @Column({type: 'jsonb'})
    changes!: object

    @Column({type: 'timestamptz', default: 'NOW()'})
    createdAt!: Date
}