import { Entity, PrimaryGeneratedColumn, Column, type Relation, ManyToMany, JoinTable, BeforeInsert, OneToMany } from "typeorm"
import { Listing } from "./Listing.js";
import bcrypt from 'bcryptjs';
import { Notification } from "./Notification.js";
import type { UserSettings } from "../../../common/src/types.js";

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id!: number

  @Column()
  login!: string

  @Column()
  password!: string

  @ManyToMany(() => Listing, { cascade: true })
  @JoinTable()
  favourites!: Promise<Relation<Listing[]>>

  @OneToMany(() => Notification, (notification) => notification.user)
  notifications!: Notification[]

  @Column({ type: 'jsonb' })
  settings!: UserSettings

  @BeforeInsert()
  async hashPassword() {
    this.password = await bcrypt.hash(this.password, 10);
  }
}
