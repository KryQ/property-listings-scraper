import { Entity, PrimaryGeneratedColumn, Column, Index, OneToMany, type Relation } from "typeorm"
import { ListingChanges } from "./ListingChanges.js";
import { ListingData } from "../../../common/src/types.js";

export const ListingSourceEnum = {
  OTODOM: 'OTODOM',
  OLX: 'OLX',
  NIERUCHOMOSCI_ONLINE: 'NIERUCHOMOSCI_ONLINE',
} as const;

export type ListingSource = (typeof ListingSourceEnum)[keyof typeof ListingSourceEnum];

const ListingTypeEnum = {
  HOUSE: 'HOUSE',
} as const;

type ListingType = (typeof ListingTypeEnum)[keyof typeof ListingTypeEnum];

@Entity()
@Index(['sourceId'])
export class Listing {
  @PrimaryGeneratedColumn()
  id!: number

  @Column({ type: 'enum', enum: ListingSourceEnum })
  source!: ListingSource

  @Column()
  sourceId!: string

  @Column()
  title!: string

  @Column({ type: 'float' })
  price!: number

  @Column({ type: 'timestamptz' })
  listingCreatedAt!: Date

  @Column({ type: 'enum', enum: ListingTypeEnum })
  type!: ListingType

  @Column()
  link!: string

  @Column({ type: 'jsonb' })
  address!: {
    city: string,
    street?: string
  } | null

  @Column({ type: 'jsonb' })
  images!: string[]

  @Column()
  shortDescription!: string

  @Column({ type: 'float', nullable: true })
  areaSquareMeters!: number | null

  @Column({ type: 'float', nullable: true })
  terrainSquareMeters!: number | null

  @Column({ default: false })
  deleted!: boolean

  @OneToMany(() => ListingChanges, (changes) => changes.listing)
  changes!: Relation<Listing>[]
}