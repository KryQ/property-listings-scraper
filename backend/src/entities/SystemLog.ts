import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

export const SystemLogLevelEnum = Object.freeze({
  INFO: 'INFO',
  WARNING: 'WARNING',
  ERROR: 'ERROR',
} as const);
export type SystemLogLevel = (typeof SystemLogLevelEnum)[keyof typeof SystemLogLevelEnum];

export const SystemLogSourceEnum = Object.freeze({
  SYSTEM: 'SYSTEM',
  SCRAPER: 'SCRAPER',
} as const);
export type SystemLogSource = (typeof SystemLogSourceEnum)[keyof typeof SystemLogSourceEnum];

@Entity()
export class SystemLog {
  @PrimaryGeneratedColumn()
  id!: number

  @Column({ type: 'enum', enum: SystemLogLevelEnum })
  level!: SystemLogLevel

  @Column({ type: 'enum', enum: SystemLogSourceEnum })
  source!: SystemLogSource

  @Column()
  message!: string

  @Column({ type: 'timestamptz', default: 'NOW()' })
  createdAt!: Date
}