import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm"
import type { Listing, ListingSource } from "./Listing.js";
import { NotificationTypeEnum, type NotificationType } from '../../../common/dist/Notification.js'
import { User } from "./User.js";

@Entity()
export class Notification {
  @PrimaryGeneratedColumn()
  id!: number

  @Column({ type: 'enum', enum: NotificationTypeEnum })
  type!: NotificationType

  @Column({ type: 'jsonb' })
  changes!: {
    source: ListingSource;
    sourceId: string;
    changeId?: number;
  }

  @Column({ type: 'timestamptz', default: 'NOW()' })
  createdAt!: Date

  @Column({ type: 'timestamptz', nullable: true })
  seenAt!: Date

  @ManyToOne(() => User, (user) => user.notifications)
  user!: User

  listing!: Listing
}