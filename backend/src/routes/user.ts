import type { FastifyError, FastifyInstance, FastifyPluginOptions } from "fastify";
import { User } from "../entities/User.js";
import bcrypt from 'bcryptjs';
import _ from "lodash";
import type { UserData, UserSettings } from '../../../common/dist/types.js'
import { UserController } from "../controllers/UserController.js";

export default async (fastify: FastifyInstance, options: FastifyPluginOptions, done: (error?: FastifyError) => void): Promise<void> => {
  fastify.route<{ Body: { token: string }, Response: { 200: UserData } }>({
    method: 'GET',
    url: '/init',
    onRequest: [fastify.authenticate],
    handler: async (request, reply) => {
      const userController = new UserController(request.user);
      reply.send(await userController.toJSON());
    }
  });

  fastify.route<{ Body: { login: string, password: string } }>({
    method: 'POST',
    url: '/login',
    handler: async (request, reply) => {
      if (fastify.db === undefined) {
        reply.status(500).send("DB_NOT_CONNECTED");
        return;
      }

      const userRepo = fastify.db.getRepository(User);
      const user = await userRepo.findOneBy({ login: request.body.login });

      if (!user) {
        reply.status(404).send();
        return;
      }

      if (!(await bcrypt.compare(request.body.password, user.password))) {
        reply.status(401).send()
        return;
      }

      const token = fastify.jwt.sign({ id: user.id })
      reply.code(200).send({ ..._.omit(user, ['password']), token })
    }
  })

  fastify.route<{ Body: { login: string, password: string }, Params: { key: string } }>({
    method: 'POST',
    url: '/create/:key',
    handler: async (request, reply) => {
      if (request.params.key !== process.env.REST_KEY) {
        reply.status(401).send();
        return;
      }

      if (fastify.db === undefined) {
        reply.status(500).send("DB_NOT_CONNECTED");
        return;
      }

      const userRepo = fastify.db.getRepository(User);

      const user = await userRepo.create({
        login: request.body.login,
        password: request.body.password,
        settings: {},
      });

      await userRepo.save(user);
    }
  })

  fastify.route<{ Body: UserSettings }>({
    method: 'PATCH',
    url: '/settings',
    onRequest: [fastify.authenticate],
    handler: async (request, reply) => {
      if (fastify.db === undefined) {
        reply.status(500).send("DB_NOT_CONNECTED");
        return;
      }

      request.user.settings = request.body;

      const userRepo = fastify.db.getRepository(User);

      await userRepo.save(request.user);

      return request.user.settings;
    },
  })
};