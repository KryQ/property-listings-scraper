import type { FastifyError, FastifyInstance, FastifyPluginOptions } from "fastify";
import { Notification } from "../entities/Notification.js";
import { Listing } from "../entities/Listing.js";
import { IsNull } from "typeorm";
import { ListingChanges } from "../entities/ListingChanges.js";

export default async (fastify: FastifyInstance, options: FastifyPluginOptions, done: (error?: FastifyError) => void): Promise<void> => {
  fastify.route({
    url: '/',
    method: 'GET',
    onRequest: [fastify.authenticate],
    handler: async (request, reply) => {
      if (fastify.db === undefined) {
        reply.status(500).send("DB_NOT_CONNECTED");
        return;
      }

      return fastify.db
        .getRepository(Notification)
        .createQueryBuilder('notification')
        .leftJoinAndMapOne("notification.listing", Listing, "listing", `listing."sourceId" = notification.changes->>'sourceId'`)
        .leftJoinAndMapOne("notification.listingChange", ListingChanges, "listingChange", `listingChange.id = (notification.changes->>'changeId')::integer`)
        .where('notification."userId" = :userId', { userId: request.user.id })
        .orderBy("notification.createdAt", "DESC")
        .limit(50)
        .getMany()
    }
  });

  fastify.route<{ Params: { id: number } }>({
    url: '/:id/seen',
    method: 'GET',
    onRequest: [fastify.authenticate],
    handler: async (request, reply) => {
      if (fastify.db === undefined) {
        reply.status(500).send("DB_NOT_CONNECTED");
        return;
      }

      const notificationRepo = fastify.db.getRepository(Notification);

      await notificationRepo.update({ id: request.params.id }, { seenAt: new Date() });

      reply.status(204);
    }
  })

  fastify.route<{ Params: { id: number } }>({
    url: '/mark-all-seen',
    method: 'GET',
    onRequest: [fastify.authenticate],
    handler: async (_, reply) => {
      if (fastify.db === undefined) {
        reply.status(500).send("DB_NOT_CONNECTED");
        return;
      }

      const notificationRepo = fastify.db.getRepository(Notification);

      await notificationRepo.update({ seenAt: IsNull() }, { seenAt: new Date() });

      reply.status(204);
    }
  })

  done();
};
