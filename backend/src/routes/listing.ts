import type { FastifyError, FastifyInstance, FastifyPluginOptions } from "fastify";
import { Listing } from "../entities/Listing.js";
import { And, Between, In, Not, Raw, type FindOptionsWhere } from "typeorm";
import { ListingChanges } from "../entities/ListingChanges.js";
import { User } from "../entities/User.js";

type whereOperators = 'not' | 'in' | 'gte' | 'gt' | 'sort' | 'lt' | 'lte' | 'like';
type listingQueryString = `${keyof Listing}.${whereOperators}` | keyof Listing

const queryStringToAbstractObject = (qs: Record<listingQueryString, unknown>): {
	where: Partial<Record<keyof Listing, string | Record<'in', string[]>>>, sort: Partial<Record<keyof Listing, unknown>>
} => {
	const where: Partial<Record<keyof Listing, unknown>> = {};
	// biome-ignore lint/suspicious/noExplicitAny: <explanation>
	const sort: Partial<Record<keyof Listing, any>> = {};

	const operatorKeyHaystack = /[\.]/;

	// biome-ignore lint/complexity/noForEach: <explanation>
	Object
		.entries(qs)
		.forEach((pairs) => {
			const key = pairs.at(0) as listingQueryString;
			const value = pairs.at(1) as unknown;

			if (operatorKeyHaystack.test(key)) {
				const field = key.split(".").at(0) as keyof Listing;
				const operator = key.split(".").at(-1) as whereOperators;

				switch (operator) {
					case 'in': {
						if (where[field]) {
							// @ts-ignore
							where[field] = { in: [...where[field].in, value] }
						} else {
							where[field] = { in: [value] }
						}
					}
						break;
					case 'not':
					case 'like':
					case 'lt':
					case 'gt': {
						if (where[field]) {
							// @ts-ignore
							where[field] = { [operator]: value, ...where[field] };
						} else {
							where[field] = { [operator]: value };
						}
					}
						break;
					case 'sort':
						sort[field] = value;
						break;
					default: console.log('unknown operator', key)
				}
			} else {
				where[(key as keyof Listing)] = value;
			}
		});

	// @ts-ignore
	return { where, sort };
}

const arrayContains = (source: unknown[], haystack: unknown[]) => haystack.every(item => source.includes(item));

const abstractObjectToWhere = (obj: Record<whereOperators, unknown>) => {
	const where: FindOptionsWhere<Listing> = {};

	const craftCompoundFieldWhere = (obj: Record<whereOperators, unknown | unknown[]>) => {
		const operators = Object.keys(obj) as whereOperators[];
		const operands = [];

		if (arrayContains(operators, ['gt', 'lt'])) {
			operands.push(Between(obj.gt, obj.lt));

			// biome-ignore lint/performance/noDelete: <explanation>
			delete obj.gt;
			// biome-ignore lint/performance/noDelete: <explanation>
			delete obj.gte;
			// biome-ignore lint/performance/noDelete: <explanation>
			delete obj.lt;
			// biome-ignore lint/performance/noDelete: <explanation>
			delete obj.lte;
		}

		Object
			.entries(obj)
			.map(pairs => {
				const key = pairs.at(0) as whereOperators;
				const value = pairs.at(1) as unknown;

				switch (key) {
					case 'in':
						operands.push(In(value as unknown[]));
						break;
					case 'not':
						operands.push(Not(value));
						break;
					case 'like': {
						operands.push(Raw((alias) => `similarity(${alias}, '${value}') > .2 or lower(${alias}) like lower('%${value}%')`));
						break;
					}
					default:
						console.log("craftCompoundFieldWhere unknown operand", key);
				}
			});

		if (operands.length > 1) {
			return And(...operands);
		}

		return operands[0];
	}

	// biome-ignore lint/complexity/noForEach: <explanation>
	Object.entries(obj)
		.forEach(pairs => {
			const key = pairs.at(0) as keyof Listing;
			const value = pairs.at(1) as unknown;

			if (typeof value === 'string') {
				where[key] = value as unknown as undefined;
			} else {
				where[key] = craftCompoundFieldWhere(value as Record<whereOperators, unknown | unknown[]>) as unknown as undefined;
			}
		})

	return where;
}

export default async (fastify: FastifyInstance, options: FastifyPluginOptions, done: (error?: FastifyError) => void): Promise<void> => {
	fastify.route<{ Querystring: Record<listingQueryString, unknown> }>({
		url: '/',
		method: 'GET',
		handler: async (request, reply) => {
			if (fastify.db === undefined) {
				reply.status(500).send("DB_NOT_CONNECTED");
				return;
			}

			const listingRepo = fastify.db.getRepository(Listing);
			const { where, sort } = queryStringToAbstractObject(request.query);

			return listingRepo.find({
				relations: { changes: true },
				// @ts-ignore
				where: abstractObjectToWhere(where),
				// @ts-ignore
				order: sort,
			});
		}
	});

	fastify.route<{ Params: { id: number } }>({
		method: 'GET',
		url: '/:id/changes',
		onRequest: [fastify.authenticate],
		handler: async (request, reply) => {
			if (fastify.db === undefined) {
				reply.status(500).send("DB_NOT_CONNECTED");
				return;
			}

			const listingChangesRepo = fastify.db.getRepository(ListingChanges);

			return listingChangesRepo.find({
				where: {
					listing: {
						id: request.params.id,
					}
				}
			})
		},
	})

	fastify.route<{ Params: { id: number } }>({
		method: 'GET',
		url: '/:id/favorite',
		onRequest: [fastify.authenticate],
		handler: async (request, reply) => {
			if (fastify.db === undefined) {
				reply.status(500).send("DB_NOT_CONNECTED");
				return;
			}

			const listingRepo = fastify.db.getRepository(Listing);
			const listing = await listingRepo.findOneBy({
				id: request.params.id
			});

			if (!listing) {
				reply.code(400).send("LISTING_NOT_FOUND");
				return;
			}

			(await request.user.favourites).push(listing);

			const userRepo = fastify.db.getRepository(User);
			await userRepo.save(request.user);

			reply.code(204).send();
			return;
		}
	});

	fastify.route<{ Params: { id: number } }>({
		method: 'GET',
		url: '/:id/unfavorite',
		onRequest: [fastify.authenticate],
		handler: async (request, reply) => {
			if (fastify.db === undefined) {
				reply.status(500).send("DB_NOT_CONNECTED");
				return;
			}

			const listingRepo = fastify.db.getRepository(Listing);
			const listing = await listingRepo.findOneBy({
				id: request.params.id
			});

			if (!listing) {
				reply.code(400).send("LISTING_NOT_FOUND");
				return;
			}

			const userFavorites = await request.user.favourites;
			const filteredFavorites = userFavorites.filter(favorite => favorite.id !== listing.id);
			request.log.fatal(filteredFavorites);
			request.user.favourites = Promise.resolve(filteredFavorites);

			const userRepo = fastify.db.getRepository(User);
			await userRepo.save(request.user);

			reply.code(204).send();
			return;
		}
	});

	done();
};
