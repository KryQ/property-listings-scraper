import type { FastifyRequest } from "fastify";
import type { User } from "./entities/User.js";

export interface FastifyRequestWithUser extends FastifyRequest {
  user: User
}