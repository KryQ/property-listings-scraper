import 'dotenv/config';
import { pino } from 'pino';
import { scrape as scrapeOlx } from "./scrapers/olx/index.js"
import { scrape as scrapeOtodom } from "./scrapers/otodom/index.js"
import { scrape as scrapeNieruchomosci } from "./scrapers/nieruchomosci-online/index.js"
import createConnection from './db.js';

const main = async () => {
  const db = await createConnection();
  const logger = pino({ level: 'debug' });

  const [olxResult, otodomResult, nieruchomosciResult] = await Promise.allSettled([
    scrapeOlx(db, logger.child({ scraper: 'OLX' })),
    scrapeOtodom(db, logger.child({ scraper: 'OTODOM' })),
    scrapeNieruchomosci(db, logger.child({ scraper: 'NIERUCHOMOSCI' }))
  ])

  console.log('olx', olxResult.status);
  console.log('otodom', otodomResult.status);
  console.log('nieruch', nieruchomosciResult.status);
}

main()
  .then(() => {
    process.exit(0);
  })
  .catch(err => {
    console.error(err);
    process.exit(1);
  })