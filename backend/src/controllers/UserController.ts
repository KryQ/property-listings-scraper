import type { User } from "../entities/User.js";
import type { UserData } from "../../../common/src/types.js";
import { ListingController } from "./ListingController.js";

export class UserController {
  private user!: User;

  constructor(user: User) {
    this.user = user;
  }

  async toJSON(): Promise<UserData> {
    return {
      id: this.user.id,
      login: this.user.login,
      settings: this.user.settings,
      favorites: await Promise.all((await this.user.favourites).map(listing => listing.id)),
    }
  }
}