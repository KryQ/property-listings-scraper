import type { DataSource, EntityManager, Repository } from "typeorm";
import { SystemLog, type SystemLogSource, type SystemLogLevel } from "../entities/SystemLog.js";

export class SystemLogController {
  private logRepo!: Repository<SystemLog>;

  constructor(db: DataSource | EntityManager) {
    this.logRepo = db.getRepository(SystemLog);
  }

  public async log(level: SystemLogLevel, source: SystemLogSource, message: string) {
    const systemLog = await this.logRepo.create({ level, source, message });

    if (!systemLog) {
      throw new Error('Cannot create systemlog!');
    }

    return this.logRepo.save(systemLog);
  }

  public async info(source: SystemLogSource, message: string): Promise<SystemLog> {
    return this.log('INFO', source, message);
  }

  public async warn(source: SystemLogSource, message: string): Promise<SystemLog> {
    return this.log('WARNING', source, message);
  }

  public async error(source: SystemLogSource, message: string): Promise<SystemLog> {
    return this.log('ERROR', source, message);
  }
}