import type { Listing } from "../entities/Listing.js";
import type { ListingData } from "../../../common/src/types.js";

export class ListingController {
  private listing!: Listing;

  constructor(listing: Listing) {
    this.listing = listing;
  }

  toJSON(): Promise<ListingData> {
    return Promise.resolve({
      id: this.listing.id,
      deleted: this.listing.deleted,
      address: this.listing.address,
      areaSquareMeters: this.listing.areaSquareMeters,
      images: this.listing.images,
      link: this.listing.link,
      listingCreatedAt: this.listing.listingCreatedAt.toISOString(),
      price: this.listing.price,
      shortDescription: this.listing.shortDescription,
      source: this.listing.source,
      sourceId: this.listing.sourceId,
      terrainSquareMeters: this.listing.terrainSquareMeters,
      title: this.listing.title,
      type: this.listing.type,
    })
  }
}