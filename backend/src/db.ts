import { DataSource } from "typeorm";
import { Listing } from "./entities/Listing.js";
import { ListingChanges } from "./entities/ListingChanges.js";
import { Notification } from "./entities/Notification.js";
import { User } from "./entities/User.js";
import { SystemLog } from "./entities/SystemLog.js";

const db = new DataSource({
	type: "postgres",
	host: process.env.DB_HOST,
	port: Number(process.env.DB_PORT),
	username: process.env.DB_USER,
	password: process.env.DB_PASSWORD,
	database: process.env.DB_NAME,
	entities: [
		Listing,
		ListingChanges,
		Notification,
		User,
		SystemLog,
	],
	synchronize: true,
	logging: ['error'],
});

const createConnection = async (): Promise<DataSource> =>
	new Promise((resolve, reject) => {
		let tries = 0;

		db.initialize()
			.then((db) => {
				resolve(db);
				return;
			})
			.catch((err) => {
				const intervalHandler = setInterval(async () => {
					tries++;
					console.log(`Trying to connect to database (try ${tries}/15)`);

					db.initialize()
						.then(connection => {
							clearInterval(intervalHandler);
							resolve(connection);
							return;
						})
						.catch(() => {
						});

					if (tries >= 15) {
						clearInterval(intervalHandler);
						reject("Could not connect to database");
						return;
					}
				}, 2000);
			});
	});

export default createConnection;
