import 'dotenv/config';
import "reflect-metadata"
import Fastify, { type FastifyReply, type FastifyRequest } from "fastify";
import FastifyStatic from "@fastify/static";
import FastifyJWT from "@fastify/jwt";
import FastifyCors from '@fastify/cors'
import { fileURLToPath } from 'node:url';
import path from 'node:path';
import { CronJob } from 'cron';
import createConnection from "./db.js";
import ListingRoute from "./routes/listing.js";
import NotificationRoute from "./routes/notification.js";
import UserRoute from "./routes/user.js";
import type { DataSource } from 'typeorm';
import { scrape as otodomScraper } from './scrapers/otodom/index.js';
import { scrape as olxScraper } from './scrapers/olx/index.js';
import { scrape as noScraper } from './scrapers/nieruchomosci-online/index.js';
import { ListingSourceEnum } from './entities/Listing.js';
import { User } from './entities/User.js';

declare module 'fastify' {
	export interface FastifyInstance {
		db: DataSource | undefined;
		authenticate: (request: FastifyRequest, reply: FastifyReply) => Promise<void>
	}
}

declare module "@fastify/jwt" {
	interface FastifyJWT {
		payload: { id: number }
		user: User
	}
}

const __dirname = path.dirname(fileURLToPath(new URL(import.meta.url)));
const isProdBuild = process.env.NODE_ENV === 'production';

const loggerTransports = {
	dev: {
		target: 'pino-pretty',
		options: {
			translateTime: 'HH:MM:ss Z',
			ignore: 'pid,hostname',
		},
	},
	production: {
		targets: [
			{
				level: 'trace',
				target: 'pino-loki',
				options: {
					batching: true,
					interval: 5,

					host: 'https://logs-prod-eu-west-0.grafana.net',
					basicAuth: {
						username: "362437",
						password: process.env.LOKI_KEY,
					},
				},
			},
			{
				level: 'warning',
				target: 'pino/file',
				options: {
					destination: 1, // 1 here represents stdout 
				},
			},
		]
	},
}

const fastify = Fastify({
	logger: {
		// @ts-ignore
		transport: loggerTransports[isProdBuild ? 'production' : 'dev'],
		name: 'property-listing',
	}
});

fastify.register(FastifyCors, {
	origin: isProdBuild ? 'https://properties.kryq.me' : '*',
})

fastify.register(FastifyStatic, {
	root: `${__dirname}/public`,
	prefix: '/public',
});

fastify.register(FastifyJWT, {
	secret: process.env.JWT_SECRET || "missing secret"
});

fastify.decorate("authenticate", async (request: FastifyRequest, reply: FastifyReply): Promise<void> => {
	try {
		await request.jwtVerify()
	} catch (err) {
		reply.send(err);
		return;
	}

	if (fastify.db === undefined) {
		reply.status(500).send("DB_NOT_CONNECTED");
		return;
	}

	const UserRepo = fastify.db.getRepository(User);

	const user = await UserRepo.findOneBy({ id: request.user.id });

	if (!user) {
		throw new Error('User does not exists');
	}

	request.user = user;
})

fastify.register(ListingRoute, { prefix: '/rest/listing' });
fastify.register(NotificationRoute, { prefix: '/rest/notification' });
fastify.register(UserRoute, { prefix: '/rest/user' });

fastify.get('/*', async (_, reply) => {
	return reply.sendFile('index.html');
});

const start = async () => {
	try {
		const db = await createConnection();
		fastify.decorate('db', db);

		// Schedule scraping only if on prod (dont want to make too many requests)
		if (isProdBuild) {
			new CronJob(
				'0 0 */2 * * *',
				async () => {
					await Promise.all([
						otodomScraper(db, fastify.log.child({ scraper: ListingSourceEnum.OTODOM })),
						olxScraper(db, fastify.log.child({ scraper: ListingSourceEnum.OLX })),
						noScraper(db, fastify.log.child({ scraper: ListingSourceEnum.NIERUCHOMOSCI_ONLINE })),
					]);
				},
				null,
				true,
				'Europe/Warsaw'
			);
		} else {
			fastify.log.debug('Skipping scheduled scraping on non prod build');
		}

		await fastify.listen({ host: '0.0.0.0', port: 3000 })
	} catch (err) {
		fastify.log.error(err)
		process.exit(1)
	}
}

start()
