export type UserData = {
  id: number
  login: string
  settings: object
  favorites?: number[]
}

export type UserDataWithAuthToken = UserData & {
  token: string
}

export type ListingData = {
  id: number
  deleted: boolean
  address: {
    city: string
    street?: string
  } | null
  areaSquareMeters: number | null
  images: string[]
  link: string
  listingCreatedAt: string
  price: number
  shortDescription: string
  source: string
  sourceId: string
  terrainSquareMeters: number | null
  title: string
  type: string
}

export type UserSettings = {
  notifications: UserNotificationSetting,
}

export const UserNotificationSettingEnum = Object.freeze({
  ALL: 'ALL',
  FAVORITE_ONLY: 'FAVORITE_ONLY',
  NONE: 'NONE'
} as const);

export type UserNotificationSetting = (typeof UserNotificationSettingEnum)[keyof typeof UserNotificationSettingEnum];