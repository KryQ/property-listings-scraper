export const NotificationTypeEnum = {
  NEW: 'NEW',
  CHANGED: 'CHANGED',
  DELETED: 'DELETED',
} as const;

export type NotificationType = (typeof NotificationTypeEnum)[keyof typeof NotificationTypeEnum];